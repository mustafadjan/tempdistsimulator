TARGET = TempDistSimulator

TEMPLATE = app

srcPath = $${PWD}/../../src
resPath = $${srcPath}/resources

TRANSLATIONS += $${resPath}/$${TARGET}_en_US.ts

CONFIG += qt debug

QT += widgets datavisualization

DESTDIR = $${OUT_PWD}/.bin
OBJECTS_DIR = $${OUT_PWD}/.obj
MOC_DIR = $${OUT_PWD}/.moc
RCC_DIR = $${OUT_PWD}/.qrc

LIBS += -L$${OUT_PWD}/../engine/.lib -lTempDistSimulatorEngine

INCLUDEPATH += $${srcPath} \
    $${srcPath}/interface \
    $${srcPath}/engine

SOURCES += \
    $${srcPath}/main_TempDistSimulator.cpp \
    $${srcPath}/interface/GUI.cpp \
    $${srcPath}/interface/Plotter.cpp \
    $${srcPath}/interface/PlotterSession.cpp \
    $${srcPath}/interface/FileWriter.cpp \
    $${srcPath}/interface/FileReader.cpp \
    $${srcPath}/interface/ReportDataWidget.cpp

HEADERS += \
    $${srcPath}/interface/GUI.h \
    $${srcPath}/interface/Plotter.h \
    $${srcPath}/interface/PlotterSession.h \
    $${srcPath}/interface/FileWriter.h \
    $${srcPath}/interface/FileReader.h \
    $${srcPath}/interface/ReportDataWidget.h

RESOURCES += $${resPath}/images.qrc

unix {
    QMAKE_POST_LINK += $$quote(lrelease $${resPath}/$${TARGET}_en_US.ts \
                               -qm $${DESTDIR}/$${TARGET}_en_US.qm)
}
win32 {
    #QMAKE_POST_LINK += копирование файла перевода в папку с исполняемым файлом программы
}
