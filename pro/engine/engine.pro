TARGET = TempDistSimulatorEngine # усложненное название для избежания коллизии имен библиотек

TEMPLATE = lib

srcPath = $${PWD}/../../src/engine

CONFIG += qt staticlib debug

QT -= gui

DESTDIR = $${OUT_PWD}/.lib
OBJECTS_DIR = $${OUT_PWD}/.obj
MOC_DIR = $${OUT_PWD}/.moc

INCLUDEPATH += $${srcPath}

SOURCES += \
    $${srcPath}/Thermodynamic.cpp \
    $${srcPath}/AbstractMathModel.cpp \
    $${srcPath}/MathModel2D.cpp \
    $${srcPath}/MathModel3D.cpp \
    $${srcPath}/MathModelSession.cpp

HEADERS += \
    $${srcPath}/Typedefs.h \
    $${srcPath}/InputData.h \
    $${srcPath}/Thermodynamic.h \
    $${srcPath}/AbstractMathModel.h \
    $${srcPath}/MathModel2D.h \
    $${srcPath}/MathModel3D.h \
    $${srcPath}/MathModelSession.h
