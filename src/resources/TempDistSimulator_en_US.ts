<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>GUI</name>
    <message>
        <location filename="../interface/GUI.cpp" line="34"/>
        <source>Размерность</source>
        <translation>Dimention</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="35"/>
        <source>Тип теплового источника</source>
        <translation>Heat Source</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="36"/>
        <source>Параметры</source>
        <translation>Parameters</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="37"/>
        <source>Материал</source>
        <translation>Material</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="82"/>
        <location filename="../interface/GUI.cpp" line="845"/>
        <source>Рассчитать</source>
        <translation>Calculate</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="83"/>
        <source>Приостановить</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="84"/>
        <source>Остановить</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="107"/>
        <source>Ночной режим</source>
        <translation>Night mode</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="132"/>
        <source>О программе</source>
        <translation>About program</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="100"/>
        <source>Моделирование</source>
        <translation>Modeling</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="68"/>
        <location filename="../interface/GUI.cpp" line="73"/>
        <location filename="../interface/GUI.cpp" line="304"/>
        <location filename="../interface/GUI.cpp" line="315"/>
        <source>Открыть файл</source>
        <translation>Open file</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="69"/>
        <location filename="../interface/GUI.cpp" line="74"/>
        <location filename="../interface/GUI.cpp" line="725"/>
        <source>Сохранить файл</source>
        <translation>Save file</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="70"/>
        <location filename="../interface/GUI.cpp" line="75"/>
        <source>Выход</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="85"/>
        <source>Отчет расчета</source>
        <translation>Report of calculation</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="94"/>
        <source>Файл</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="141"/>
        <location filename="../interface/GUI.cpp" line="652"/>
        <source>Показать настройки</source>
        <translation>Show settings</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="159"/>
        <source>Текущий высотный уровень</source>
        <translation>Current height level</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="165"/>
        <source>Готово</source>
        <translation>Done</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="194"/>
        <source>Двумерный расчет</source>
        <translation>2d-calculation</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="195"/>
        <source>Трехмерный расчет</source>
        <translation>3d-calculation</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="196"/>
        <source>Точечный</source>
        <translation>Point</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="197"/>
        <source>Линейный</source>
        <translation>Line</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="198"/>
        <source>Эллиптический</source>
        <translation>Ellipse</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="211"/>
        <source>Количество шагов по
пространству (макс. - 300)</source>
        <translation>Number of steps
by range (max. - 300)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="212"/>
        <source>Количество шагов по
времени (макс. - 1000)</source>
        <translation>Number of steps
by time (max. - 1000)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="213"/>
        <source>Ширина, (м)</source>
        <translation>Width, (K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="214"/>
        <source>Длина, (м)</source>
        <translation>Lenght, (K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="215"/>
        <source>Высота, (м)</source>
        <translation>Height, (K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="216"/>
        <source>Начальная температура
объекта, (K)</source>
        <translation>Initial temperature
of object, (K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="217"/>
        <source>Температура окружающей
среды, (K)</source>
        <translation>Temperature of
around area, (K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="218"/>
        <source>Время процесса, (с)</source>
        <translation>Process time, (s)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="234"/>
        <source>Медь</source>
        <translation>Copper</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="235"/>
        <source>Серебро</source>
        <translation>Silver</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="236"/>
        <source>Золото</source>
        <translation>Gold</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="237"/>
        <source>Цинк</source>
        <translation>Zinc</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="238"/>
        <source>Алюминий</source>
        <translation>Aluminium</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="239"/>
        <source>Олово</source>
        <translation>Tin</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="240"/>
        <source>Свинец</source>
        <translation>Lead</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="241"/>
        <source>Железо</source>
        <translation>Iron</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="242"/>
        <source>Кобальт</source>
        <translation>Cobalt</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="243"/>
        <source>Никель</source>
        <translation>Nickel</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="250"/>
        <source>Другой материал</source>
        <translation>Other material</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="251"/>
        <source>При выборе другого материала, теплофизические характеристики будут постоянными</source>
        <translation>Selecting this option will make thermophysical characteristics constant</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="607"/>
        <source>Скрыть настройки</source>
        <translation>Hide Settings</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="379"/>
        <source>Применение режима</source>
        <translation>Application mode</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="219"/>
        <source>Удельная мощность
источника теплового
излучения, (Вт/м^2)</source>
        <translation>Specific source power
of thermal emission,
(W/m^2)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="267"/>
        <source>Плотность материала, (кг/м^3)</source>
        <translation>Heat capacity of material, (kg/m^3)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="268"/>
        <source>Удельная теплоемкость материала, (Дж/(кг*K))</source>
        <translation>Specific heat of material, (J/(kg*K))</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="269"/>
        <source>Теплопроводность материала, (Вт/(м*K))</source>
        <translation>Thermal conductivity of material, W/(m*K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="305"/>
        <source>Расчет еще не закончен, вы уверены, что хотите открыть файл? Ваш текущий расчет будет завершен.</source>
        <translation>Calculation is not finished yet, are you sure you want to open file? Your current calculation will be end.</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="307"/>
        <location filename="../interface/GUI.cpp" line="717"/>
        <source>Да</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="308"/>
        <location filename="../interface/GUI.cpp" line="718"/>
        <source>Нет</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="356"/>
        <source>Данная программа позволяет узнать распределение теплоты в прямоугольном объекте (двумерном или трехмерном) при заданных параметрах при воздействии тепла с одной стороны.

Все параметры неотрицательны.
Заданное количество шагов по простраству соответствует наибольшей стороне, в остальных сторонах количество шагов будет соответсвовать их длине.
Объект находится в состоянии покоя, окруженный неподвижным воздухом, коэффицент теплоотдачи для каждого материала (металла) принимается равным 5,6 (Вт/(м^2*К)).
Для каждого из имеющихся в списке материалов (металлов) теплофизические характеристики являются динамическими (функциями от температур), для другого материала теплофизические характеристики задаются вручную и являются постоянными (не зависят от температуры).
Тип теплового источника определяет тип теплового воздействия на одну сторону пластины, точечный действует как безразмерная точка в центр стороны, линейный действует как линия размером в половину ширины стороны с одинаковым воздействием в каждой точке, эллиптический действует как эллипс с максимальным воздействием в центре и постепенным снижением к краям.

2018. Мустафин Антон Рушанович.</source>
        <translation>This program allows you to know the distribution of heat in a rectangular object (2d or 3d) for specified parameters when heat is applied to one side.

All parameters are nonnegative.
The specified number of steps for prostration corresponds to the largest side, in other parts the number of steps will correspond to their length.
The object is at rest, surrounded by fixed air, the heat transfer coefficient for each material (metal) is taken to be 5,6 (W/(m^2*K)).
For each of the materials (metals) in the list, the thermophysical characteristics are dynamic (functions of temperatures), for another material, the thermal physical characteristics are set manually and are constant (independent of temperature).
The type of heat source determines the type of thermal action on one side of the plate, the point source acts as a dimensionless point at the center of the side, the linear acts as a line half the width of the side with the same effect at each point, the elliptic acts as an ellipse with maximum impact at the center and gradual decrease to edges.

2018. Mustafin Anton Rushanovich.

P.S. Sorry for my english.</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="380"/>
        <source>Для применения режима необходимо перезапустить программу.</source>
        <translation>Mode change will take effect after restart.</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="135"/>
        <source>Помощь</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="270"/>
        <source>Температура плавления материала, (K)</source>
        <translation>Melting point of material, (K)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="714"/>
        <source>Запись в файл</source>
        <translation>Writing to file</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="715"/>
        <source>Расчет еще не закончен, вы уверены, что хотите сохранить файл? Ваш текущий расчет будет завершен.</source>
        <translation>Calculation is not finished yet, are you sure you want to save file? Your current calculation will be end.</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="732"/>
        <source>Файл не сохранен</source>
        <translation>File not saved</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="732"/>
        <source>Неправильное имя файла.</source>
        <translation>Wrong name of file.</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="768"/>
        <source> секунд)</source>
        <translation> seconds)</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="789"/>
        <source>Достигнута температура плавления материала,
дальнейший расчет невозможен.</source>
        <translation>Melting point of material is reached,
further calculation is not possible.</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="806"/>
        <source>Возобновить</source>
        <translation>Resume</translation>
    </message>
    <message>
        <location filename="../interface/GUI.cpp" line="355"/>
        <source>О </source>
        <translation>About </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../interface/FileReader.cpp" line="18"/>
        <location filename="../interface/FileReader.cpp" line="26"/>
        <source>Невозможно открыть файл</source>
        <translation>Can&apos;t open file</translation>
    </message>
    <message>
        <location filename="../interface/FileReader.cpp" line="19"/>
        <source>Невозможно открыть файл,
возможно файла с заданным
именем не существет</source>
        <translation>Can&apos;t open file,
perhapsfile wth that
name doesn&apos;t exist</translation>
    </message>
    <message>
        <location filename="../interface/FileReader.cpp" line="27"/>
        <source>Файл не является сохраненным
файлом данной программы</source>
        <translation>File is not a saved
file of this program</translation>
    </message>
</context>
<context>
    <name>ReportDataWidget</name>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="11"/>
        <source>Отчет расчета</source>
        <translation>Report of calculation</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="18"/>
        <source>двумерный расчет</source>
        <translation>2d-calculation</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="21"/>
        <source>трехмерный расчет</source>
        <translation>3d-calculation</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="26"/>
        <source>точечный</source>
        <translation>point</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="29"/>
        <source>линейный</source>
        <translation>line</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="32"/>
        <source>эллиптический</source>
        <translation>ellipse</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="37"/>
        <source>медь</source>
        <translation>copper</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="40"/>
        <source>серебро</source>
        <translation>silver</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="43"/>
        <source>золото</source>
        <translation>gold</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="46"/>
        <source>цинк</source>
        <translation>zinc</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="49"/>
        <source>алюминий</source>
        <translation>aluminium</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="52"/>
        <source>олово</source>
        <translation>tin</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="55"/>
        <source>свинец</source>
        <translation>lead</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="58"/>
        <source>железо</source>
        <translation>iron</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="61"/>
        <source>кобальт</source>
        <translation>cobalt</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="64"/>
        <source>никель</source>
        <translation>nickel</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="67"/>
        <source>другой материал</source>
        <translation>other material</translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="70"/>
        <source>Размерность: </source>
        <translation>Dimention: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="71"/>
        <source>Тип теплового источника: </source>
        <translation>Heat Source: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="72"/>
        <source>Количество шагов по ширине: </source>
        <translation>Number of steps by width: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="74"/>
        <source>Количество шагов по длине: </source>
        <translation>Number of steps by length: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="77"/>
        <source>Количество шагов по высоте: </source>
        <translation>Number of steps by height: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="80"/>
        <source>Количество шагов по времени: </source>
        <translation>Number of steps by time: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="82"/>
        <source>Ширина, (м): </source>
        <translation>Width, (m): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="84"/>
        <source>Длина, (м): </source>
        <translation>Lenght, (m): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="87"/>
        <source>Высота, (м): </source>
        <translation>Height, (m): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="90"/>
        <source>Начальная температура объекта, (K): </source>
        <translation>Initial temperature of object, (K): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="92"/>
        <source>Температура окружающей среды, (K): </source>
        <translation>Temperature of around area, (K): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="94"/>
        <source>Время процесса, (с): </source>
        <translation>Process time, (s): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="96"/>
        <source>Удельная мощность источника
теплового излучения, (Вт/м^2): </source>
        <translation>Specific source power of
thermal emission, (W/m^2): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="98"/>
        <source>Материал: </source>
        <translation>Material: </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="100"/>
        <source>Плотность материала, (кг/м^3): </source>
        <translation>Heat capacity of material, (kg/m^3): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="102"/>
        <source>Удельная теплоемкость материала, (Дж/(кг*K)): </source>
        <translation>Specific heat of material, (J/(kg*K)): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="104"/>
        <source>Теплопроводность материала, (Вт/(м*K)): </source>
        <translation>Thermal conductivity of material, W/(m*K): </translation>
    </message>
    <message>
        <location filename="../interface/ReportDataWidget.cpp" line="106"/>
        <source>Температура плавления материала, (K): </source>
        <translation>Melting point of material, (K): </translation>
    </message>
</context>
</TS>
