#include "FileReader.h"
#include "InputData.h"
#include "PlotterSession.h"
#include "Plotter.h"
#include <QJsonObject>
#include <QFile>
#include <QMessageBox>
#include <QJsonDocument>
#include <QJsonArray>
//-------------------------------------------------------------------------------------------------
FileReader::FileReader(const QString& crStrOpenFileName):
  _pJsonObject(new QJsonObject),
  _bIsReaded(false)
{
  QFile oFile(crStrOpenFileName);
  // проверка правильности имени файла
  if (oFile.exists() && !oFile.open(QIODevice::ReadOnly)) {
    QMessageBox::critical(nullptr, QObject::tr("Невозможно открыть файл"),
      QObject::tr("Невозможно открыть файл,\nвозможно файла с заданным\nименем не существет"));
    return;
  }
  // считывание всех данных из файла
  *_pJsonObject = QJsonDocument::fromBinaryData(oFile.readAll()).object();
  // проверка подписи файла
  if (!_pJsonObject->contains("TempDistSimulatorMAIAntonMustafin")) {
    QMessageBox::critical(nullptr, QObject::tr("Невозможно открыть файл"),
                          QObject::tr("Файл не является сохраненным\nфайлом данной программы"));
    return;
  }
  _bIsReaded = true;
}
//-------------------------------------------------------------------------------------------------
FileReader::~FileReader()
{
  delete _pJsonObject;
}
//-------------------------------------------------------------------------------------------------
std::shared_ptr<TempDistSimulatorEngine::InputData> FileReader::getInputData() const
{
  auto pInputData = std::make_shared<TempDistSimulatorEngine::InputData>();
  // считывание основных параметров
  if (_pJsonObject->contains("enDimType") && _pJsonObject->value("enDimType").isDouble()) {
    pInputData->_enDimType = static_cast<TempDistSimulatorEngine::EnDimType>
                              (_pJsonObject->value("enDimType").toInt());
  }
  if (_pJsonObject->contains("enQwType") && _pJsonObject->value("enQwType").isDouble()) {
    pInputData->_enQwType = static_cast<TempDistSimulatorEngine::EnQwType>
                             (_pJsonObject->value("enQwType").toInt());
  }
  if (_pJsonObject->contains("enMaterialType")&&_pJsonObject->value("enMaterialType").isDouble()) {
    pInputData->_enMaterialType = static_cast<TempDistSimulatorEngine::EnMaterialType>
                                   (_pJsonObject->value("enMaterialType").toInt());
  }
  if (_pJsonObject->contains("usNObjWidth") && _pJsonObject->value("usNObjWidth").isDouble()) {
    pInputData->_usNObjWidth = static_cast<quint16>(_pJsonObject->value("usNObjWidth").toInt());
  }
  if (_pJsonObject->contains("usNObjLenght") && _pJsonObject->value("usNObjLenght").isDouble()) {
    pInputData->_usNObjLenght = static_cast<quint16>(_pJsonObject->value("usNObjLenght").toInt());
  }
  if (_pJsonObject->contains("usNTime") && _pJsonObject->value("usNTime").isDouble()) {
    pInputData->_usNTime = static_cast<quint16>(_pJsonObject->value("usNTime").toInt());
  }
  if (_pJsonObject->contains("dObjWidth") && _pJsonObject->value("dObjWidth").isDouble()) {
    pInputData->_dObjWidth = _pJsonObject->value("dObjWidth").toDouble();
  }
  if (_pJsonObject->contains("dObjLenght") && _pJsonObject->value("dObjLenght").isDouble()) {
    pInputData->_dObjLenght = _pJsonObject->value("dObjLenght").toDouble();
  }
  if (_pJsonObject->contains("dObjTemp") && _pJsonObject->value("dObjTemp").isDouble()) {
    pInputData->_dObjTemp = _pJsonObject->value("dObjTemp").toDouble();
  }
  if (_pJsonObject->contains("dAreaTemp") && _pJsonObject->value("dAreaTemp").isDouble()) {
    pInputData->_dAreaTemp = _pJsonObject->value("dAreaTemp").toDouble();
  }
  if (_pJsonObject->contains("dTime") && _pJsonObject->value("dTime").isDouble()) {
    pInputData->_dTime = _pJsonObject->value("dTime").toDouble();
  }
  if (_pJsonObject->contains("dPowerDensity") && _pJsonObject->value("dPowerDensity").isDouble()) {
    pInputData->_dPowerDensity = _pJsonObject->value("dPowerDensity").toDouble();
  }
  // считывание уникальных параметров для типа "Свой материал"
  if (pInputData->_enMaterialType == TempDistSimulatorEngine::EnMaterialType::enMaterialOther) {
    if (_pJsonObject->contains("dObjDensity") && _pJsonObject->value("dObjDensity").isDouble()) {
      pInputData->_dObjDensity = _pJsonObject->value("dObjDensity").toDouble();
    }
    if (_pJsonObject->contains("dObjHeatCapacity") && _pJsonObject->value("dObjHeatCapacity").
        isDouble()) {
      pInputData->_dObjHeatCapacity = _pJsonObject->value("dObjHeatCapacity").toDouble();
    }
    if (_pJsonObject->contains("dObjThermalConductivity") &&
        _pJsonObject->value("dObjThermalConductivity").isDouble()) {
      pInputData->_dObjThermalConductivity = _pJsonObject->value("dObjThermalConductivity").
        toDouble();
    }
    if (_pJsonObject->contains("dObjFusionTemp") && _pJsonObject->value("dObjFusionTemp").
        isDouble()) {
      pInputData->_dObjFusionTemp = _pJsonObject->value("dObjFusionTemp").toDouble();
    }
  }
  // считывание уникальныого параметра для трехмерного расчета
  if (pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
    if (_pJsonObject->contains("usNObjHeight") && _pJsonObject->value("usNObjHeight").isDouble()) {
      pInputData->_usNObjHeight=static_cast<quint16>(_pJsonObject->value("usNObjHeight").toInt());
    }
  }
  return pInputData;
}
//-------------------------------------------------------------------------------------------------
PlotterSession* FileReader::getPlotData(QtDataVisualization::Q3DSurface* p3DSurface,
                       const std::shared_ptr<TempDistSimulatorEngine::InputData> pInputData) const
{
  auto pPlotterSession = new PlotterSession;
  pPlotterSession->vAddThread(p3DSurface, pInputData);

  // считывание двумерного массива
  if (pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim2D) {
    for (int j = 0; j <= pInputData->_usNObjLenght; ++j) {
      QJsonArray oJsonArray;
      if (_pJsonObject->contains(QString::number(j)) && _pJsonObject->value(QString::number(j)).
          isArray()) {
        oJsonArray = _pJsonObject->value(QString::number(j)).toArray();
      }
      for (int i = 0; i <= pInputData->_usNObjWidth; ++i) {
        pPlotterSession->getPlotter()->vSetTXY(i, j, oJsonArray[i].toDouble());
      }
    }
    emit pPlotterSession->getPlotter()->signalUpdateFinishedPlot(- 1);
  }
  // считывание трехмерного массива
  if (pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
    for (int k = 0; k <= pInputData->_usNObjHeight; ++k) {
      QJsonArray oJsonArray;
      if (_pJsonObject->contains(QString::number(k)) && _pJsonObject->value(QString::number(k)).
          isArray()) {
        oJsonArray = _pJsonObject->value(QString::number(k)).toArray();
      }
      for (int j = 0; j <= pInputData->_usNObjLenght; ++j) {
        QJsonObject oJsonObject = oJsonArray[j].toObject();
        for (int i = 0; i <= pInputData->_usNObjWidth; ++i) {
          if (oJsonObject.contains(QString::number(k)) && oJsonObject[QString::number(k)].
              isDouble()) {
            pPlotterSession->getPlotter()->vSetTXYZ(i, j, k,
                                                    oJsonObject[QString::number(k)].toDouble());
          }
        }
      }
    }
    emit pPlotterSession->getPlotter()->signalUpdateFinishedPlot(pInputData->_usNObjHeight);
  }
  return pPlotterSession;
}
//-------------------------------------------------------------------------------------------------
