#pragma once
//-------------------------------------------------------------------------------------------------
#include <memory>
#include <QMainWindow>
class QSpinBox;
class QGroupBox;
class QLineEdit;
class PlotterSession;
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
  struct InputData;
}
namespace QtDataVisualization {
  class Q3DSurface;
}
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс графического пользовательского интерфейса
 */
class GUI final : public QMainWindow
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit GUI(const bool, QWidget* pParent = nullptr);

  /** @brief Деструктор */
  ~GUI();

private:

  /** @brief Прочитать данные с формы */
  std::shared_ptr<TempDistSimulatorEngine::InputData> getInputDataFromForm() const;

  /** @brief Получить выбранный переключатель из группы */
  int iGetSelectedItem(QGroupBox* pGroupBox = nullptr) const;

  /** @brief Прочитать настройки */
  void vReadSettings() const;

  /** @brief Сохранить настройки */
  void vWriteSettings() const;

  /** @brief Действия после прочтения файла */
  void vPostFileRead(QAction*, QAction*, QSpinBox*) const;

  /** @brief Установка анимации свертывания/развертывания виджета всех настрек */
  void vSetSlideAnimation(QWidget*, QWidget*, QAction*, QPixmap*, QPixmap*);

  /** @brief Установка расчета */
  void vSetCalc(QWidget*, QAction*, QAction*, QAction*, QAction*, QAction*, QAction*, QSpinBox*);

  /** @brief Флаг о статусе расчета */
  bool _bIsCalcRunning;

  /** @brief Позиция виджета всех настроек в предыдущей итерации анимации */
  QPoint _oPointPrevPos;

  /** @brief Указатель на группу "Размерность" */
  QGroupBox* _pGroupBoxDimention;

  /** @brief Указатель на группу "Тип теплового источника" */
  QGroupBox* _pGroupBoxHeatSource;

  /** @brief Указатель на группу "Параметры" */
  QGroupBox* _pGroupBoxParameters;

  /** @brief Указатель на группу "Материал" */
  QGroupBox* _pGroupBoxMaterial;

  /** @brief Указатель на поле ввода "Количество шагов по пространству" */
  QLineEdit* _pLineEditNDist;

  /** @brief Указатель на поле ввода "Количество шагов по времени" */
  QLineEdit* _pLineEditNTime;

  /** @brief Указатель на поле ввода "Ширина, (м)" */
  QLineEdit* _pLineEditObjWidth;

  /** @brief Указатель на поле ввода "Длина, (м)" */
  QLineEdit* _pLineEditObjLenght;

  /** @brief Указатель на поле ввода "Высота, (м)" */
  QLineEdit* _pLineEditObjHeight;

  /** @brief Указатель на поле ввода "Начальная температура объекта, (K)" */
  QLineEdit* _pLineEditObjTemp;

  /** @brief Указатель на поле ввода "Температура окружающей среды, (K)" */
  QLineEdit* _pLineEditAreaTemp;

  /** @brief Указатель на поле ввода "Время процесса, (с)" */
  QLineEdit* _pLineEditTime;

  /** @brief Указатель на поле ввода "Мощность источника теплового излучения, (Вт/м^2)" */
  QLineEdit* _pLineEditPowerDensity;

  /** @brief Указатель на поле ввода "ρ" */
  QLineEdit* _pLineEditObjDensity;

  /** @brief Указатель на поле ввода "c" */
  QLineEdit* _pLineEditHeatCapacity;

  /** @brief Указатель на поле ввода "λ" */
  QLineEdit* _pLineEditThermalConductivity;

  /** @brief Указатель на поле ввода "Tλ" */
  QLineEdit* _pLineEditFusionTemp;

  /** @brief Указатель на область построения графиков */
  QtDataVisualization::Q3DSurface* _p3DSurface;

  /** @brief Указатель на строитель графиков */
  PlotterSession* _pPlotterSession;

  /** @brief Указатель на считываемые данные */
  std::shared_ptr<TempDistSimulatorEngine::InputData> _pInputData;

private slots:

  /** @brief Изменение высотного уровня */
  void slotChangeHeightLevel(int);

signals:

  /** @brief Возобновление процедуры сохранения/загрузки после завершения расчета */
  void signalResumeRW();

};
//-------------------------------------------------------------------------------------------------
