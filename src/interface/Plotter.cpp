#include "Plotter.h"
#include "InputData.h"
#include <cfloat>
#include <Q3DSurface>
//-------------------------------------------------------------------------------------------------
Plotter::Plotter(QtDataVisualization::Q3DSurface* p3DSurface,
                 const std::shared_ptr<TempDistSimulatorEngine::InputData> cpInputData, QObject* pParent):
  QObject(pParent),
  _bIsFinished(false),
  _p3DSurface(p3DSurface)
{
  const auto cpSurface3DSeries = new QtDataVisualization::QSurface3DSeries;
  p3DSurface->addSeries(cpSurface3DSeries);
  // расцветка графика по температуре
  QLinearGradient oLinearGradient;
  oLinearGradient.setColorAt(0.0,   Qt::darkGreen);
  oLinearGradient.setColorAt(0.333, Qt::yellow);
  oLinearGradient.setColorAt(0.667, Qt::red);
  oLinearGradient.setColorAt(1.0,   Qt::darkRed);
  cpSurface3DSeries->setBaseGradient(oLinearGradient);
  cpSurface3DSeries->setColorStyle(QtDataVisualization::Q3DTheme:: ColorStyleRangeGradient);
  // шаги по осям
  const double cdXStep = cpInputData->_dObjWidth / static_cast<double>(cpInputData->_usNObjWidth),
  cdYStep = cpInputData->_dObjLenght / static_cast<double>(cpInputData->_usNObjLenght);
  // расчет глубины дробной части шагов по осям для шкал
  double dWidth{.0}, dLenght{.0}, dCoef{cdXStep};
  quint8 usX{0}, usY{0};
  while (dCoef < 1.) {
    dCoef *= 10.;
    ++usX;
  }
  dCoef = cdYStep;
  while (dCoef < 1.) {
    dCoef *= 10.;
    ++usY;
  }
  p3DSurface->axisX()->setLabelFormat("%." + QString::number(usX) + "f m");
  p3DSurface->axisY()->setLabelFormat("%.1f K");
  p3DSurface->axisZ()->setLabelFormat("%." + QString::number(usY) + "f m");
  p3DSurface->axisX()->setRange(- FLT_EPSILON, cpInputData->_dObjWidth);
  p3DSurface->axisZ()->setRange(- FLT_EPSILON, cpInputData->_dObjLenght);
  const int ciWidth{cpInputData->_usNObjWidth + 1}, ciLenght{cpInputData->_usNObjLenght + 1};
  const auto cpSurfaceDataArray = new QtDataVisualization::QSurfaceDataArray;
  cpSurfaceDataArray->reserve(ciWidth);
  for (int i = 0; i < ciWidth; ++i) {
    cpSurfaceDataArray->append(new QtDataVisualization::QSurfaceDataRow(ciLenght));
    for (int j = 0; j < ciLenght; ++j) {
      (*(*cpSurfaceDataArray)[i])[j].setX(dWidth);
      (*(*cpSurfaceDataArray)[i])[j].setZ(dLenght);
      dLenght += cdYStep;
    }
    dLenght = .0;
    dWidth += cdXStep;
  }
  cpSurface3DSeries->dataProxy()->resetArray(cpSurfaceDataArray);

  // обновление данных на графике расчета
  connect(this, &Plotter::signalUpdatePlot, this, &Plotter::slotUpdatePlot);
  _dTXY.resize(cpInputData->_usNObjWidth + 1);
  _dTXY.fill(QVector<double>(cpInputData->_usNObjLenght + 1));
  if (cpInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
    _dTXYZ.resize(cpInputData->_usNObjWidth + 1);
    _dTXYZ.fill(QVector<QVector<double>>(cpInputData->_usNObjLenght + 1));
    for (int i = 0; i < cpInputData->_usNObjWidth + 1; ++i) {
      _dTXYZ[i].fill(QVector<double>(cpInputData->_usNObjHeight + 1));
    }
  }
  // обновление данных на графике завершенного расчета (смена высотного уровня)
  connect(this, &Plotter::signalUpdateFinishedPlot, [this, cpInputData] (const int iLevel)
  {
    // срез высотного уровня
    if (iLevel >= 0) {
      for (quint16 i = 0; i <= cpInputData->_usNObjWidth; ++i) {
        for (quint16 j = 0; j <= cpInputData->_usNObjLenght; ++j) {
          _dTXY[i][j] = _dTXYZ[i][j][iLevel];
        }
      }
    }
    slotUpdatePlot(&_dTXY);
  });
  // лямбда перезаписи трехмерного массива при окончании расчета
  connect(this, &Plotter::signalWriteArray3D, [this]
          (const TempDistSimulatorEngine::ArrayDouble3D* cdTXYZ)
  {
    _dTXYZ = *cdTXYZ;
    emit signalFinishWriteArray3D();
  });
}
//-------------------------------------------------------------------------------------------------
Plotter::~Plotter()
{
  const auto cpSurface3DSeries = _p3DSurface->seriesList().last();
  _p3DSurface->removeSeries(cpSurface3DSeries);
  delete cpSurface3DSeries;
}
//-------------------------------------------------------------------------------------------------
const TempDistSimulatorEngine::ArrayDouble2D* Plotter::getTXY()
{
  const auto cpListArray = _p3DSurface->seriesList().last()->dataProxy()->array();
  int iWidth{cpListArray->size()}, iLenght{cpListArray->at(0)->size()};
  for (int i = 0; i < iWidth; ++i) {
    for (int j = 0; j < iLenght; ++j) {
      _dTXY[i][j] = (*(*cpListArray)[i])[j].y();
    }
  }
  return &_dTXY;
}
//-------------------------------------------------------------------------------------------------
void Plotter::slotUpdatePlot(const TempDistSimulatorEngine::ArrayDouble2D* cdTXY, bool bIsFinished)
{
  quint16 usMinTemp{static_cast<quint16>((*cdTXY)[0][0] + FLT_EPSILON)}, usMaxTemp{0};
  for (int i = 0; i < cdTXY->size(); ++i) {
    for (int j = 0; j < (*cdTXY)[i].size(); ++j) {
      (*(*_p3DSurface->seriesList().last()->dataProxy()->array())[i])[j].setY((*cdTXY)[i][j]);
      // поиск минимальной и максимальной температур
      if (usMinTemp > (*cdTXY)[i][j]) {
        usMinTemp = static_cast<quint16>((*cdTXY)[i][j] + FLT_EPSILON);
      }
      if (usMaxTemp < (*cdTXY)[i][j]) {
        usMaxTemp = static_cast<quint16>((*cdTXY)[i][j]);
      }
    }
  }
  _bIsFinished = bIsFinished;
  if (bIsFinished) {
    emit signalFinishPlot();
  }
  usMinTemp -= usMinTemp % 100;
  usMaxTemp += 100 - (usMaxTemp % 100);
  if (usMinTemp == usMaxTemp) {
    usMaxTemp += 100;
  }
  _p3DSurface->axisY()->setRange(usMinTemp, usMaxTemp);
  emit _p3DSurface->seriesList().last()->dataProxy()->arrayReset();
}
//-------------------------------------------------------------------------------------------------
