#include "FileWriter.h"
#include "InputData.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QJsonDocument>
//-------------------------------------------------------------------------------------------------
FileWriter::FileWriter(const QString& crStrSaveFileName,
                       const std::shared_ptr<TempDistSimulatorEngine::InputData> cpInputData,
                       const TempDistSimulatorEngine::ArrayDouble2D* cdTXY)
{
  QJsonObject oJsonObject;
  vInit(oJsonObject, cpInputData);
  // запись двумерного массива
  for (int j = 0; j <= cpInputData->_usNObjLenght; ++j) {
    QJsonArray oJsonArray;
    for (int i = 0; i <= cpInputData->_usNObjWidth; ++i) {
      oJsonArray.append((*cdTXY)[i][j]);
    }
    oJsonObject[QString::number(j)] = oJsonArray;
  }
  vSaveToFile(crStrSaveFileName, oJsonObject);
}
//-------------------------------------------------------------------------------------------------
FileWriter::FileWriter(const QString& crStrSaveFileName,
                       const std::shared_ptr<TempDistSimulatorEngine::InputData> cpInputData,
                       const TempDistSimulatorEngine::ArrayDouble3D* cdTXYZ)
{
  QJsonObject oJsonObject;
  vInit(oJsonObject, cpInputData);
  // запись уникальных параметров для трехмерного расчета
  oJsonObject["usNObjHeight"] = cpInputData->_usNObjHeight;
  oJsonObject["dObjHeight"] = cpInputData->_dObjHeight;
  // запись трехмерного массива
  for (int k = 0; k <= cpInputData->_usNObjHeight; ++k) {
    QJsonArray oJsonArray;
    for (int j = 0; j <= cpInputData->_usNObjLenght; ++j) {
      QJsonObject oJsonObject;
      for (int i = 0; i <= cpInputData->_usNObjWidth; ++i) {
        oJsonObject[QString::number(i)] = (*cdTXYZ)[i][j][k];
      }
      oJsonArray.append(oJsonObject);
    }
    oJsonObject[QString::number(k)] = oJsonArray;
  }
  vSaveToFile(crStrSaveFileName, oJsonObject);
}
//-------------------------------------------------------------------------------------------------
void FileWriter::vInit(QJsonObject& rJsonObject,
                       const std::shared_ptr<TempDistSimulatorEngine::InputData> cpInputData) const
{
  // подпись файла
  rJsonObject["TempDistSimulatorMAIAntonMustafin"] = true;
  // запись основных параметров
  rJsonObject["enDimType"] = static_cast<int>(cpInputData->_enDimType);
  rJsonObject["enQwType"] = static_cast<int>(cpInputData->_enQwType);
  rJsonObject["enMaterialType"] = static_cast<int>(cpInputData->_enMaterialType);
  rJsonObject["usNObjWidth"] = cpInputData->_usNObjWidth;
  rJsonObject["usNObjLenght"] = cpInputData->_usNObjLenght;
  rJsonObject["usNTime"] = cpInputData->_usNTime;
  rJsonObject["dObjWidth"] = cpInputData->_dObjWidth;
  rJsonObject["dObjLenght"] = cpInputData->_dObjLenght;
  rJsonObject["dObjTemp"] = cpInputData->_dObjTemp;
  rJsonObject["dAreaTemp"] = cpInputData->_dAreaTemp;
  rJsonObject["dTime"] = cpInputData->_dTime;
  rJsonObject["dPowerDensity"] = cpInputData->_dPowerDensity;
  // запись уникальных параметров для типа "Свой материал"
  if (cpInputData->_enMaterialType == TempDistSimulatorEngine::EnMaterialType::enMaterialOther) {
    rJsonObject["dObjDensity"] = cpInputData->_dObjDensity;
    rJsonObject["dObjHeatCapacity"] = cpInputData->_dObjHeatCapacity;
    rJsonObject["dObjThermalConductivity"] = cpInputData->_dObjThermalConductivity;
    rJsonObject["dObjFusionTemp"] = cpInputData->_dObjFusionTemp;
  }
}
//-------------------------------------------------------------------------------------------------
void FileWriter::vSaveToFile(const QString& crStrSaveFileName,const QJsonObject& crJsonObject)const
{
  QFile oFile(crStrSaveFileName);
  oFile.open(QIODevice::WriteOnly);
  QJsonDocument oJsonDocument(crJsonObject);
  oFile.write(oJsonDocument.toBinaryData());
}
//-------------------------------------------------------------------------------------------------
