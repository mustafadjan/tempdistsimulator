#pragma once
#include <memory>
//-------------------------------------------------------------------------------------------------
class QString;
class QJsonObject;
class PlotterSession;
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
  struct InputData;
}
namespace QtDataVisualization {
  class Q3DSurface;
}
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс чтения результатов расчета из файла
 */
class FileReader final
{
public:

  /** @brief Конструктор */
  explicit FileReader(const QString&);

  /** @brief Деструктор */
  ~FileReader();

  /** @brief Прочитать признак считывания файла */
  bool bIsReaded() const { return _bIsReaded; }

  /** @brief Чтение входных данных из файла */
  std::shared_ptr<TempDistSimulatorEngine::InputData> getInputData() const;

  /** @brief Чтение данных построения графика из файла */
  PlotterSession* getPlotData(QtDataVisualization::Q3DSurface*,
                              const std::shared_ptr<TempDistSimulatorEngine::InputData>) const;

private:

  /** @brief Указатель на json-объект считываемого файла */
  QJsonObject* _pJsonObject;

  /** @brief Признак считывания файла */
  bool _bIsReaded;

};
//-------------------------------------------------------------------------------------------------
