#pragma once
//-------------------------------------------------------------------------------------------------
#include "Typedefs.h"
#include <memory>
#include <QObject>
//-------------------------------------------------------------------------------------------------
namespace QtDataVisualization {
  class Q3DSurface;
}
namespace TempDistSimulatorEngine {
  class InputData;
}
class Plotter;
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс сессии построения графика
 * @details Построение проходит в отдельном потоке
 */
class PlotterSession final : public QObject
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit PlotterSession(QObject* pParent = nullptr);

  /** @brief Деструктор */
  ~PlotterSession();

  /** @brief Добавление потока построения графика */
  void vAddThread(QtDataVisualization::Q3DSurface*,
                  const std::shared_ptr<TempDistSimulatorEngine::InputData>);

  /** @brief Получить указатель области построения графика */
  Plotter* getPlotter() { return _pPlotter; }

private:

  /** @brief Указатель области построения графика */
  Plotter* _pPlotter;

signals:

  /** @brief Завершение обновления данных */
  void signalUpdatePlotDone() const;

  /** @brief Завершение перезаписи трехмерного массива */
  void signalFinishWriteArray3D() const;

};
//-------------------------------------------------------------------------------------------------
