#include "GUI.h"
#include "InputData.h"
#include "PlotterSession.h"
#include "Plotter.h"
#include "MathModelSession.h"
#include "AbstractMathModel.h"
#include "MathModel3D.h"
#include "FileReader.h"
#include "FileWriter.h"
#include "ReportDataWidget.h"
#include <QCoreApplication>
#include <QSpinBox>
#include <QGroupBox>
#include <QLineEdit>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QBoxLayout>
#include <QFormLayout>
#include <Q3DSurface>
#include <QRadioButton>
#include <QLabel>
#include <QValidator>
#include <QMessageBox>
#include <QFileDialog>
#include <QPropertyAnimation>
#include <QSettings>
#include <QProgressBar>
//-------------------------------------------------------------------------------------------------
GUI::GUI(const bool cbIsNightMode, QWidget* pParent):
  QMainWindow(pParent),
  _bIsCalcRunning(false),
  _oPointPrevPos(QPoint()),
  _pGroupBoxDimention          (new QGroupBox(tr("Размерность"), this)),
  _pGroupBoxHeatSource         (new QGroupBox(tr("Тип теплового источника"), this)),
  _pGroupBoxParameters         (new QGroupBox(tr("Параметры"), this)),
  _pGroupBoxMaterial           (new QGroupBox(tr("Материал"), _pGroupBoxParameters)),
  _pLineEditNDist              (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditNTime              (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditObjWidth           (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditObjLenght          (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditObjHeight          (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditObjTemp            (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditAreaTemp           (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditTime               (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditPowerDensity       (new QLineEdit(_pGroupBoxParameters)),
  _pLineEditObjDensity         (new QLineEdit(_pGroupBoxMaterial)),
  _pLineEditHeatCapacity       (new QLineEdit(_pGroupBoxMaterial)),
  _pLineEditThermalConductivity(new QLineEdit(_pGroupBoxMaterial)),
  _pLineEditFusionTemp         (new QLineEdit(_pGroupBoxMaterial)),
  _p3DSurface                  (new QtDataVisualization::Q3DSurface),
  _pPlotterSession             (nullptr),
  _pInputData                  (nullptr)
{
  // установка основных компонентов
  setWindowTitle(QCoreApplication::applicationName());
  setWindowIcon(QPixmap(":/TempDistIcon.png"));
  setCentralWidget(new QWidget(this));
  setMenuBar(new QMenuBar(this));
  setContextMenuPolicy(Qt::NoContextMenu);
  // изображения для действий
  static QPixmap oPixmapInfoButton, oPixmapMenu, oPixmapArrowLeft;
  // действия чтения/записи в файл и выхода
  #ifdef Q_OS_LINUX
  const QIcon coIconOpen = QIcon::fromTheme("document-open");
  const QIcon coIconSave = QIcon::fromTheme("document-save");
  const QIcon coIconExit = QIcon::fromTheme("application-exit");
  const auto cpActionOpenFile = new QAction(coIconOpen, tr("Открыть файл"), this);
  const auto cpActionSaveFile = new QAction(coIconSave, tr("Сохранить файл"), this);
  const auto cpActionExit     = new QAction(coIconExit, tr("Выход"), this);
  #endif
  #ifdef Q_OS_WIN32
  const auto cpActionOpenFile = new QAction(tr("Открыть файл"), this);
  const auto cpActionSaveFile = new QAction(tr("Сохранить файл"), this);
  const auto cpActionExit     = new QAction(tr("Выход"), this);
  #endif
  cpActionOpenFile->setShortcut(QKeySequence::Open);
  cpActionSaveFile->setShortcut(QKeySequence::Save);
  cpActionExit    ->setShortcut(QKeySequence::Quit);
  cpActionSaveFile->setEnabled(false);
  // указатели на действия "Рассчитать", "Приостановить", "Остановить" и "Отчет расчета"
  const auto cpActionCalc   = new QAction(QPixmap(":/StartButton.png"), tr("Рассчитать"), this);
  const auto cpActionPause  = new QAction(QPixmap(":/PauseButton.png"), tr("Приостановить"), this);
  const auto cpActionStop   = new QAction(QPixmap(":/StopButton.png"),  tr("Остановить"), this);
  const auto cpActionReport = new QAction(QPixmap(":/ReportButton.png"),tr("Отчет расчета"), this);
  cpActionCalc  ->setShortcut(Qt::Key_Space);
  cpActionPause ->setShortcut(Qt::Key_Space);
  cpActionStop  ->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_S);
  cpActionReport->setShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_R);
  cpActionPause ->setEnabled(false);
  cpActionStop  ->setEnabled(false);
  cpActionReport->setEnabled(false);
  // указатель на меню "Файл"
  const auto cpMenuFile = menuBar()->addMenu(tr("Файл"));
  cpMenuFile->addAction(cpActionOpenFile);
  cpMenuFile->addAction(cpActionSaveFile);
  cpMenuFile->addSeparator();
  cpMenuFile->addAction(cpActionExit);
  // указатель на меню "Моделирование"
  const auto cpMenuModeling = menuBar()->addMenu(tr("Моделирование"));
  cpMenuModeling->addAction(cpActionCalc);
  cpMenuModeling->addAction(cpActionPause);
  cpMenuModeling->addAction(cpActionStop);
  cpMenuModeling->addAction(cpActionReport);
  // указатели на действия "Ночной режим" и "О программе"
  #ifdef Q_OS_LINUX
  const auto cpActionNightMode = new QAction(tr("Ночной режим"), this);
  cpActionNightMode->setEnabled(false);
  cpActionNightMode->setObjectName("cpActionNightMode");
  cpActionNightMode->setCheckable(true);
  cpActionNightMode->setShortcut(Qt::CTRL + Qt::Key_N);
  // применение "Ночного режима"
  cpActionNightMode->setChecked(cbIsNightMode);
  if (cbIsNightMode) {
    _p3DSurface->setActiveTheme(new QtDataVisualization::Q3DTheme(QtDataVisualization::Q3DTheme::
                                                                  ThemeStoneMoss, _p3DSurface));
    oPixmapInfoButton.load(":/InfoButtonNight.png");
    oPixmapMenu.load(":/MenuNight.png");
    oPixmapArrowLeft.load(":/ArrowLeftNight.png");
  }
  else {
    oPixmapInfoButton.load(":/InfoButton.png");
    oPixmapMenu.load(":/Menu.png");
    oPixmapArrowLeft.load(":/ArrowLeft.png");
  }
  #endif
  #ifdef Q_OS_WIN32
  oPixmapInfoButton.load(":/InfoButton.png");
  oPixmapMenu.load(":/Menu.png");
  oPixmapArrowLeft.load(":/ArrowLeft.png");
  #endif
  const auto cpActionAbout = new QAction(oPixmapInfoButton, tr("О программе"), this);
  cpActionAbout->setShortcut(QKeySequence::HelpContents);
  // указатель на меню "Помощь"
  const auto cpMenuHelp = menuBar()->addMenu(tr("Помощь"));
  #ifdef Q_OS_LINUX
  cpMenuHelp->addAction(cpActionNightMode);
  #endif
  cpMenuHelp->addAction(cpActionAbout);
  // указатель на действие "Показать настройки"
  const auto cpActionSettings = new QAction(oPixmapMenu, tr("Показать настройки"), this);
  // указатель на панель инструментов
  const auto cpToolBar = new QToolBar(this);
  cpToolBar->setMovable(false);
  cpToolBar->addAction(cpActionSettings);
  #ifdef Q_OS_LINUX
  cpToolBar->addAction(cpActionOpenFile);
  cpToolBar->addAction(cpActionSaveFile);
  #endif
  cpToolBar->addSeparator();
  cpToolBar->addAction(cpActionCalc);
  cpToolBar->addAction(cpActionPause);
  cpToolBar->addAction(cpActionStop);
  cpToolBar->addAction(cpActionReport);
  cpToolBar->addSeparator();
  cpToolBar->addAction(cpActionAbout);
  // указатели на действие и поле ввода выбора высотного уровня
  const auto cpSpinBox = new QSpinBox(cpToolBar);
  cpSpinBox->setToolTip(tr("Текущий высотный уровень"));
  cpToolBar->addSeparator();
  const auto cpActionLevel = cpToolBar->addWidget(cpSpinBox);
  cpActionLevel->setVisible(false);
  addToolBar(Qt::TopToolBarArea, cpToolBar);
  setStatusBar(new QStatusBar(this));
  statusBar()->showMessage(tr("Готово"), 10000);

  // установка главной разметки на центральный виджет
  const auto cpHBoxLayoutMain = new QHBoxLayout(centralWidget());
  // установка виджета всех настроек (при старте спрятан)
  const auto cpWidgetSettings = new QWidget(centralWidget());
  cpWidgetSettings->hide();
  // установка разметки для левой области окна на главной разметке
  const auto cpVBoxLayoutLeft = new QVBoxLayout(cpWidgetSettings);
  cpVBoxLayoutLeft->setContentsMargins(QMargins());
  // установка виджета области построения графиков
  _p3DSurface->setShadowQuality(QtDataVisualization::QAbstract3DGraph::ShadowQualityNone);
  const auto cpWidget3DSurface = QWidget::createWindowContainer(_p3DSurface, this);
  cpWidget3DSurface->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  cpWidget3DSurface->setMinimumSize(800, 600);
  cpHBoxLayoutMain->addWidget(cpWidget3DSurface);
  // установка разметки для размерности и типа источника на разметке левой области окна
  const auto cpHBoxLayoutDementionHeatSource = new QHBoxLayout;
  cpVBoxLayoutLeft->addLayout(cpHBoxLayoutDementionHeatSource);
  // установка группировок кнопок
  _pGroupBoxDimention ->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  _pGroupBoxHeatSource->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  _pGroupBoxParameters->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
  cpHBoxLayoutDementionHeatSource->addWidget(_pGroupBoxDimention);
  cpHBoxLayoutDementionHeatSource->addWidget(_pGroupBoxHeatSource);
  const auto cpVBoxLayoutDemention  = new QVBoxLayout(_pGroupBoxDimention);
  const auto cpVBoxLayoutHeatSource = new QVBoxLayout(_pGroupBoxHeatSource);
  cpVBoxLayoutLeft->addWidget(_pGroupBoxParameters);
  // установка переключателей размерности и типа теплового источника
  const auto cpRadioButton2D      = new QRadioButton(tr("Двумерный расчет"),  _pGroupBoxDimention);
  const auto cpRadioButton3D      = new QRadioButton(tr("Трехмерный расчет"), _pGroupBoxDimention);
  const auto cpRadioButtonPoint   = new QRadioButton(tr("Точечный"),         _pGroupBoxHeatSource);
  const auto cpRadioButtonLine    = new QRadioButton(tr("Линейный"),         _pGroupBoxHeatSource);
  const auto cpRadioButtonEllipse = new QRadioButton(tr("Эллиптический"),    _pGroupBoxHeatSource);
  cpVBoxLayoutDemention ->addWidget(cpRadioButton2D);
  cpVBoxLayoutDemention ->addWidget(cpRadioButton3D);
  cpVBoxLayoutHeatSource->addWidget(cpRadioButtonPoint);
  cpVBoxLayoutHeatSource->addWidget(cpRadioButtonLine);
  cpVBoxLayoutHeatSource->addWidget(cpRadioButtonEllipse);
  // установка разметки для параметров
  const auto cpHBoxLayoutParameters = new QHBoxLayout(_pGroupBoxParameters);
  const auto cpVBoxLayoutParametersLeft = new QVBoxLayout;
  cpHBoxLayoutParameters->addLayout(cpVBoxLayoutParametersLeft);
  _pGroupBoxMaterial->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
  cpHBoxLayoutParameters->addWidget(_pGroupBoxMaterial);
  // установка названий параметров левой области
  new QLabel(tr("Количество шагов по\nпространству (макс. - 300)"),    _pLineEditNDist);
  new QLabel(tr("Количество шагов по\nвремени (макс. - 1000)"),        _pLineEditNTime);
  new QLabel(tr("Ширина, (м)"),                                        _pLineEditObjWidth);
  new QLabel(tr("Длина, (м)"),                                         _pLineEditObjLenght);
  new QLabel(tr("Высота, (м)"),                                        _pLineEditObjHeight);
  new QLabel(tr("Начальная температура\nобъекта, (K)"),                _pLineEditObjTemp);
  new QLabel(tr("Температура окружающей\nсреды, (K)"),                 _pLineEditAreaTemp);
  new QLabel(tr("Время процесса, (с)"),                                _pLineEditTime);
  new QLabel(tr("Удельная мощность\nисточника теплового\nизлучения, (Вт/м^2)"),
             _pLineEditPowerDensity);
  // установка валидации полей ввода параметров левой области
  _pLineEditNDist->setValidator(new QIntValidator(2, 300, _pLineEditNDist));
  _pLineEditNTime->setValidator(new QIntValidator(1, 1000, _pLineEditNTime));
  // группировка названий и полей ввода
  for (const auto& cpLabel : _pGroupBoxParameters->findChildren<QLabel*>()) {
    if (cpVBoxLayoutParametersLeft->count()) {
      cpVBoxLayoutParametersLeft->addStretch();
    }
    const auto cpLineEdit = cpLabel->parentWidget();
    cpVBoxLayoutParametersLeft->addWidget(cpLabel);
    cpVBoxLayoutParametersLeft->addWidget(cpLineEdit);
  }
  // установка переключателей материалов
  new QRadioButton(tr("Медь"),     _pGroupBoxMaterial);
  new QRadioButton(tr("Серебро"),  _pGroupBoxMaterial);
  new QRadioButton(tr("Золото"),   _pGroupBoxMaterial);
  new QRadioButton(tr("Цинк"),     _pGroupBoxMaterial);
  new QRadioButton(tr("Алюминий"), _pGroupBoxMaterial);
  new QRadioButton(tr("Олово"),    _pGroupBoxMaterial);
  new QRadioButton(tr("Свинец"),   _pGroupBoxMaterial);
  new QRadioButton(tr("Железо"),   _pGroupBoxMaterial);
  new QRadioButton(tr("Кобальт"),  _pGroupBoxMaterial);
  new QRadioButton(tr("Никель"),   _pGroupBoxMaterial);
  // установка разметки переключателей материалов
  const auto cpVBoxLayoutMaterial = new QVBoxLayout(_pGroupBoxMaterial);
  for (const auto& cpRadioButton : _pGroupBoxMaterial->findChildren<QRadioButton*>()) {
    cpVBoxLayoutMaterial->addWidget(cpRadioButton);
  }
  cpVBoxLayoutMaterial->addStretch();
  const auto cpRadioButtonOther = new QRadioButton(tr("Другой материал"), _pGroupBoxMaterial);
  cpRadioButtonOther->setStatusTip(tr("При выборе другого материала, теплофизические "
                                      "характеристики будут постоянными"));
  cpVBoxLayoutMaterial->addWidget(cpRadioButtonOther);
  // установка полей ввода физических параметров для другого материала
  const auto cpFormLayoutSelfParams = new QFormLayout;
  cpVBoxLayoutMaterial->addLayout(cpFormLayoutSelfParams);
  new QLabel("ρ",  _pLineEditObjDensity);
  new QLabel("c",  _pLineEditHeatCapacity);
  new QLabel("λ",  _pLineEditThermalConductivity);
  new QLabel("Tλ", _pLineEditFusionTemp);
  const auto vSetSelfMaterialLineEditsAccess = [this] (bool b) {
    for (const auto& cpLineEdit : _pGroupBoxMaterial->findChildren<QLineEdit*>()) {
      cpLineEdit->setEnabled(b);
    }
  };
  vSetSelfMaterialLineEditsAccess(false);
  _pLineEditObjDensity         ->setStatusTip(tr("Плотность материала, (кг/м^3)"));
  _pLineEditHeatCapacity       ->setStatusTip(tr("Удельная теплоемкость материала, (Дж/(кг*K))"));
  _pLineEditThermalConductivity->setStatusTip(tr("Теплопроводность материала, (Вт/(м*K))"));
  _pLineEditFusionTemp         ->setStatusTip(tr("Температура плавления материала, (K)"));
  // установка всплывающего окна подсказки для всех названий формы и добавление их вместе с
  // родительскими полями ввода в группировку
  for (const auto& cpLabel : _pGroupBoxMaterial->findChildren<QLabel*>()) {
    cpLabel->setStatusTip(cpLabel->parentWidget()->statusTip());
    cpFormLayoutSelfParams->addRow(cpLabel, cpLabel->parentWidget());
  }
  // установка политики минимального размера и отсутствия фокуса для всех переключателей формы
  for (const auto& cpRadioButton : findChildren<QRadioButton*>()) {
    cpRadioButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    cpRadioButton->setFocusPolicy(Qt::NoFocus);
  }
  // установка политики минимального размера для всех названий формы
  for (const auto& cpLabel : findChildren<QLabel*>()) {
    cpLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  }
  // установка политики минимального размера и "сильного" фокуса для всех полей ввода формы и
  // установка валидатора double чисел с точкой! и 3 цифрами после нее для некоторых полей ввода
  for (const auto& cpLineEdit : findChildren<QLineEdit*>()) {
    cpLineEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    cpLineEdit->setFocusPolicy(Qt::StrongFocus);
    if (!cpLineEdit->validator()) {
      cpLineEdit->setValidator(new QRegExpValidator(QRegExp("[+]?\\d*[\\.]?\\d{1,3}"),cpLineEdit));
    }
  }

  // лямбда загрузки расчета из файла
  connect(cpActionOpenFile, &QAction::triggered, [this, cpActionSaveFile, cpActionStop,
                                                  cpActionReport, cpActionLevel, cpSpinBox]
  {
    // окно вопроса загрузки при незавершенном расчете
    if (_bIsCalcRunning) {
      QMessageBox oMessageBoxOpen;
      oMessageBoxOpen.setIcon(QMessageBox::Question);
      oMessageBoxOpen.setWindowTitle(tr("Открыть файл"));
      oMessageBoxOpen.setText(tr("Расчет еще не закончен, вы уверены, что хотите открыть "
                                 "файл? Ваш текущий расчет будет завершен."));
      oMessageBoxOpen.addButton(tr("Да"), QMessageBox::YesRole);
      oMessageBoxOpen.addButton(tr("Нет"), QMessageBox::NoRole);
      if (oMessageBoxOpen.exec()) {
        return;
      }
      cpActionStop->activate(QAction::Trigger);
    }
    // считывание введенного названия файла
    const auto coStrOpenFileName = QFileDialog::getOpenFileName(nullptr, tr("Открыть файл"), "",
                                                                "*.dat");
    if (!coStrOpenFileName.isEmpty()) {
      cpActionSaveFile->setEnabled(false);
      // ожидание завершения расчета
      if (_bIsCalcRunning) {
        _pPlotterSession->getPlotter()->vSetFinished(true);
        QEventLoop oEventLoopUntilMath;
        connect(this, &GUI::signalResumeRW, &oEventLoopUntilMath, &QEventLoop::quit);
        oEventLoopUntilMath.exec();
      }
      const FileReader coFileReader(coStrOpenFileName);
      if (coFileReader.bIsReaded()) {
        if (_pPlotterSession) {
          delete _pPlotterSession;
        }
        _pInputData = coFileReader.getInputData();
        _pPlotterSession = coFileReader.getPlotData(_p3DSurface, _pInputData);
        vPostFileRead(cpActionReport, cpActionLevel, cpSpinBox);
      }
    }
  });

  // закрыть программу
  connect(cpActionExit, &QAction::triggered, this, &GUI::close);

  // установка анимации свертывания/развертывания виджета всех настрек
  vSetSlideAnimation(cpWidgetSettings, cpWidget3DSurface, cpActionSettings,
                     &oPixmapMenu, &oPixmapArrowLeft);

  // изменение высотного уровня
  connect(cpSpinBox, SIGNAL(valueChanged(int)), this, SLOT(slotChangeHeightLevel(int)));

  // установка расчета
  vSetCalc(cpWidgetSettings, cpActionSaveFile, cpActionCalc, cpActionPause, cpActionStop,
           cpActionReport, cpActionLevel, cpSpinBox);

  // лямбда вывода окна информации "О программе"
  connect(cpActionAbout, &QAction::triggered, [this]
  {
    QMessageBox::about(this, tr("О ") + QCoreApplication::applicationName() + " (" +
                       QCoreApplication::applicationVersion() + ")", tr(
      "Данная программа позволяет узнать распределение теплоты в прямоугольном объекте (двумерном "
      "или трехмерном) при заданных параметрах при воздействии тепла с одной стороны.\n\n"
      "Все параметры неотрицательны.\n"
      "Заданное количество шагов по простраству соответствует наибольшей стороне, в остальных "
      "сторонах количество шагов будет соответсвовать их длине.\n"
      "Объект находится в состоянии покоя, окруженный неподвижным воздухом, коэффицент теплоотдачи"
      " для каждого материала (металла) принимается равным 5,6 (Вт/(м^2*К)).\n"
      "Для каждого из имеющихся в списке материалов (металлов) теплофизические характеристики "
      "являются динамическими (функциями от температур), для другого материала теплофизические "
      "характеристики задаются вручную и являются постоянными (не зависят от температуры).\n"
      "Тип теплового источника определяет тип теплового воздействия на одну сторону пластины, "
      "точечный действует как безразмерная точка в центр стороны, линейный действует как линия "
      "размером в половину ширины стороны с одинаковым воздействием в каждой точке, эллиптический "
      "действует как эллипс с максимальным воздействием в центре и постепенным снижением к "
      "краям.\n\n"
      "2018. Мустафин Антон Рушанович."));
  });

  #ifdef Q_OS_LINUX
  // лямбда включения "Ночного режима"
  connect(cpActionNightMode, &QAction::triggered, []
  {
    QMessageBox::information(nullptr, tr("Применение режима"),
                             tr("Для применения режима необходимо перезапустить программу."));
  });
  #endif

  // выключение поля ввода "Высота, (м)" при двумерном расчете
  connect(cpRadioButton2D, &QRadioButton::toggled, _pLineEditObjHeight, &QLineEdit::setDisabled);
  // включение полей ввода "ρ", "c", "λ" и "Tλ" при выборе другого материала
  connect(cpRadioButtonOther, &QRadioButton::toggled, vSetSelfMaterialLineEditsAccess);

  vReadSettings();
  showMaximized();
}
//-------------------------------------------------------------------------------------------------
GUI::~GUI()
{
  vWriteSettings();
  if (_pPlotterSession) {
    delete _pPlotterSession;
  }
  // удаление области построения графиков обеспечивает также удаление серии этой области,
  // прокси данных этой серии, а также массива данных этих прокси данных
  delete _p3DSurface;
}
//-------------------------------------------------------------------------------------------------
std::shared_ptr<TempDistSimulatorEngine::InputData> GUI::getInputDataFromForm() const
{
  auto pInputData = std::make_shared<TempDistSimulatorEngine::InputData>();
  pInputData->_enDimType = static_cast<TempDistSimulatorEngine::EnDimType>(iGetSelectedItem
                                                                            (_pGroupBoxDimention));
  pInputData->_enQwType = static_cast<TempDistSimulatorEngine::EnQwType>(iGetSelectedItem
                                                                          (_pGroupBoxHeatSource));
  pInputData->_enMaterialType = static_cast<TempDistSimulatorEngine::EnMaterialType>
                                                            (iGetSelectedItem(_pGroupBoxMaterial));
  // распределение значений количества шагов по сторонам согласно их значениям
  QMap<quint8, float> mapSides;
  mapSides[0] = _pLineEditObjWidth->text().toFloat();
  mapSides[1] = _pLineEditObjLenght->text().toFloat();
  if (pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
    mapSides[2] = _pLineEditObjHeight->text().toFloat();
  }
  auto itMapSides = mapSides.begin();
  float fMaxSide{0.}, fCoef{0.};
  while (itMapSides != mapSides.end()) {
    if (itMapSides.value() > fMaxSide) {
      fMaxSide = itMapSides.value();
    }
    ++itMapSides;
  }

  while (!mapSides.empty()) {
    itMapSides = mapSides.begin();
    ++itMapSides;
    auto itMaxMapSides = mapSides.begin();
    while (itMapSides != mapSides.end()) {
      if (itMapSides.value() > itMaxMapSides.value()) {
        itMaxMapSides = itMapSides;
      }
      ++itMapSides;
    }
    switch (itMaxMapSides.key()) {
      case 0:
        fCoef = itMaxMapSides.value() / fMaxSide;
        pInputData->_usNObjWidth = static_cast<quint16>(_pLineEditNDist->text().toFloat() *
                                                         fCoef + .0001f);
        pInputData->_dObjWidth = static_cast<double>(itMaxMapSides.value());
        mapSides.erase(itMaxMapSides);
        break;
      case 1:
        fCoef = itMaxMapSides.value() / fMaxSide;
        pInputData->_usNObjLenght = static_cast<quint16>(_pLineEditNDist->text().toFloat() *
                                                          fCoef + .0001f);
        pInputData->_dObjLenght = static_cast<double>(itMaxMapSides.value());
        mapSides.erase(itMaxMapSides);
        break;
      case 2:
        if (pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
          fCoef = itMaxMapSides.value() / fMaxSide;
          pInputData->_usNObjHeight = static_cast<quint16>(_pLineEditNDist->text().toFloat() *
                                                            fCoef + .0001f);
          pInputData->_dObjHeight = static_cast<double>(itMaxMapSides.value());
        }
        mapSides.erase(itMaxMapSides);
    }
  }
  // страховка от ошибок, возникающих при шаге по пространству < 2
  if (pInputData->_usNObjWidth < 2) {
    pInputData->_usNObjWidth = 2;
  }
  if (pInputData->_usNObjLenght < 2) {
    pInputData->_usNObjLenght = 2;
  }
  if (pInputData->_usNObjHeight < 2 &&
      pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
    pInputData->_usNObjHeight = 2;
  }
  pInputData->_usNTime       = _pLineEditNTime       ->text().toUShort();
  pInputData->_dObjTemp      = _pLineEditObjTemp     ->text().toDouble();
  pInputData->_dAreaTemp     = _pLineEditAreaTemp    ->text().toDouble();
  pInputData->_dTime         = _pLineEditTime        ->text().toDouble();
  pInputData->_dPowerDensity = _pLineEditPowerDensity->text().toDouble();
  if (pInputData->_enMaterialType == TempDistSimulatorEngine::EnMaterialType::enMaterialOther) {
    pInputData->_dObjDensity             = _pLineEditObjDensity         ->text().toDouble();
    pInputData->_dObjHeatCapacity        = _pLineEditHeatCapacity       ->text().toDouble();
    pInputData->_dObjThermalConductivity = _pLineEditThermalConductivity->text().toDouble();
    pInputData->_dObjFusionTemp          = _pLineEditFusionTemp         ->text().toDouble();
  }
  return pInputData;
}
//-------------------------------------------------------------------------------------------------
int GUI::iGetSelectedItem(QGroupBox* pGroupBox) const
{
  if (pGroupBox) {
    const auto listRadioButtons = pGroupBox->findChildren<QRadioButton*>();
    int i = 0;
    while (i != listRadioButtons.size()) {
      if (listRadioButtons.at(i)->isChecked()) {
        return i;
      }
      ++i;
    }
  }
  return -1;
}
//-------------------------------------------------------------------------------------------------
void GUI::vReadSettings() const
{
  QSettings oSettings;
  oSettings.beginGroup("/Settings");
  _pGroupBoxDimention ->findChildren<QRadioButton*>().at(oSettings.value("Dimention",  0).toInt())
                                                         ->setChecked(true);
  _pGroupBoxHeatSource->findChildren<QRadioButton*>().at(oSettings.value("HeatSource", 0).toInt())
                                                         ->setChecked(true);
  _pGroupBoxMaterial  ->findChildren<QRadioButton*>().at(oSettings.value("Material",   0).toInt())
                                                         ->setChecked(true);
  _pLineEditNDist->setText              (oSettings.value("NDist", "300")              .toString());
  _pLineEditNTime->setText              (oSettings.value("NTime", "600")              .toString());
  _pLineEditObjWidth->setText           (oSettings.value("ObjWidth", "0.15")          .toString());
  _pLineEditObjLenght->setText          (oSettings.value("ObjLenght", "0.15")         .toString());
  _pLineEditObjHeight->setText          (oSettings.value("ObjHeight", "0.05")         .toString());
  _pLineEditObjTemp->setText            (oSettings.value("ObjTemp", "300")            .toString());
  _pLineEditAreaTemp->setText           (oSettings.value("AreaTemp", "300")           .toString());
  _pLineEditTime->setText               (oSettings.value("Time", "10")                .toString());
  _pLineEditPowerDensity->setText       (oSettings.value("PowerDensity", "5000000")   .toString());
  _pLineEditObjDensity->setText         (oSettings.value("ObjDensity", "10000")       .toString());
  _pLineEditHeatCapacity->setText       (oSettings.value("HeatCapacity", "250")       .toString());
  _pLineEditThermalConductivity->setText(oSettings.value("ThermalConductivity", "500").toString());
  _pLineEditFusionTemp->setText         (oSettings.value("FusionTemp", "400")         .toString());
  oSettings.endGroup();
}
//-------------------------------------------------------------------------------------------------
void GUI::vWriteSettings() const
{
  QSettings oSettings;
  oSettings.beginGroup("/Settings");
  oSettings.setValue("Dimention",           iGetSelectedItem(_pGroupBoxDimention));
  oSettings.setValue("HeatSource",          iGetSelectedItem(_pGroupBoxHeatSource));
  oSettings.setValue("Material",            iGetSelectedItem(_pGroupBoxMaterial));
  oSettings.setValue("NDist",               _pLineEditNDist              ->text().toUShort());
  oSettings.setValue("NTime",               _pLineEditNTime              ->text().toUShort());
  oSettings.setValue("ObjWidth",            _pLineEditObjWidth           ->text());
  oSettings.setValue("ObjLenght",           _pLineEditObjLenght          ->text());
  oSettings.setValue("ObjHeight",           _pLineEditObjHeight          ->text());
  oSettings.setValue("ObjTemp",             _pLineEditObjTemp            ->text());
  oSettings.setValue("AreaTemp",            _pLineEditAreaTemp           ->text());
  oSettings.setValue("Time",                _pLineEditTime               ->text());
  oSettings.setValue("PowerDensity",        _pLineEditPowerDensity       ->text());
  oSettings.setValue("ObjDensity",          _pLineEditObjDensity         ->text());
  oSettings.setValue("HeatCapacity",        _pLineEditHeatCapacity       ->text());
  oSettings.setValue("ThermalConductivity", _pLineEditThermalConductivity->text());
  oSettings.setValue("FusionTemp",          _pLineEditFusionTemp         ->text());
  #ifdef Q_OS_LINUX
  oSettings.setValue("NightMode",           findChild<QAction*>("cpActionNightMode")->isChecked());
  #endif
  oSettings.endGroup();
}
//-------------------------------------------------------------------------------------------------
void GUI::vPostFileRead(QAction* pActionReport, QAction* pActionLevel, QSpinBox* pSpinBox) const
{
  // виджет отчета по расчету
  const auto cpReportDataWidget = std::make_shared<ReportDataWidget>(_pInputData);
  pActionReport->setEnabled(true);
  // лямбда отчета после завершения расчета
  connect(pActionReport, &QAction::triggered, _pPlotterSession, [cpReportDataWidget]
  {
    cpReportDataWidget->exec();
  });
  // настройка поля ввода выбора высотного уровня
  if (_pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
    pActionLevel->setVisible(true);
    pSpinBox->setRange(0, _pInputData->_usNObjHeight);
    pSpinBox->setValue(pSpinBox->maximum());
  }
  else {
    pActionLevel->setVisible(false);
  }
}
//-------------------------------------------------------------------------------------------------
void GUI::vSetSlideAnimation(QWidget* cpWidgetSettings, QWidget* cpWidget3DSurface,
                             QAction* cpActionSettings,
                             QPixmap* pPixmapMenu, QPixmap* pPixmapArrowLeft)
{
  // настройка анимации свертывания/развертывания виджета всех настрек
  const auto cpPropertyAnimationHideSlide = new QPropertyAnimation(cpWidgetSettings, "pos", this);
  cpPropertyAnimationHideSlide->setDuration(500);

  // лямбда перемещения поля построения графика синхронно с виджетом всех настроек
  connect(cpPropertyAnimationHideSlide, &QPropertyAnimation::valueChanged,
          [this, cpWidget3DSurface] (const QVariant& crPointNewPos)
  {
    QPoint oPointChange = crPointNewPos.toPoint() - _oPointPrevPos;
    QSize oSizeNew(cpWidget3DSurface->width() - oPointChange.x(), cpWidget3DSurface->height());
    cpWidget3DSurface->setGeometry(QRect(cpWidget3DSurface->pos() + oPointChange, oSizeNew));
    _oPointPrevPos = crPointNewPos.toPoint();
  });

  // лямбда старта анимации свертывания/развертывания виджета всех настрек
  // (не стоит использовать как хрестоматийную, т.к. написана с учетом постоянной ширины виджета
  // всех настроек)
  connect(cpActionSettings, &QAction::triggered, [this, cpActionSettings,
          cpPropertyAnimationHideSlide, cpWidgetSettings, cpWidget3DSurface, pPixmapArrowLeft]
  {
    // фиксация размера на время работы анимации для избежания некорректного применения размеров
    setFixedSize(size());
    // развертывание виджета всех настроек
    if (cpPropertyAnimationHideSlide->direction() == QPropertyAnimation::Forward) {
      if (cpPropertyAnimationHideSlide->state() == QPropertyAnimation::Stopped) {
        // поменять действие "Показать настройки" на "Скрыть настройки" с изменением иконки
        cpActionSettings->setText(tr("Скрыть настройки"));
        cpActionSettings->setIcon(*pPixmapArrowLeft);
        // стартовое положение виджета всех настроек
        QSize oSize(cpWidgetSettings->sizeHint().width(), cpWidget3DSurface->height());
        QPoint oPoint(- oSize.width() + (centralWidget()->layout()->spacing() / 2),
                      cpWidget3DSurface->pos().y());
        cpWidgetSettings->setGeometry(QRect(oPoint, oSize));
        cpWidgetSettings->show();
        _oPointPrevPos = oPoint;
        cpPropertyAnimationHideSlide->setStartValue(oPoint);
        cpPropertyAnimationHideSlide->setEndValue(QPoint(cpWidget3DSurface->pos()));
        cpPropertyAnimationHideSlide->setEasingCurve(QEasingCurve::InQuad);
        cpPropertyAnimationHideSlide->start();
      }
      else {
        cpPropertyAnimationHideSlide->setDirection(QPropertyAnimation::Backward);
      }
    }
    // свертывание виджета всех настроек
    else {
      if (cpPropertyAnimationHideSlide->state() == QPropertyAnimation::Stopped) {
        cpPropertyAnimationHideSlide->setEasingCurve(QEasingCurve::OutQuad);
        cpPropertyAnimationHideSlide->start();
      }
      else {
        cpPropertyAnimationHideSlide->setDirection(QPropertyAnimation::Forward);
      }
    }
  });

  // лямбда настроек при завершении анимации свертывания/развертывания виджета всех настрек
  connect(cpPropertyAnimationHideSlide, &QPropertyAnimation::finished,
          [this, cpPropertyAnimationHideSlide, cpWidgetSettings, cpActionSettings, pPixmapMenu]
  {
    // расфиксация размера
    setFixedSize(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
    if (cpPropertyAnimationHideSlide->direction() == QPropertyAnimation::Forward) {
      cpPropertyAnimationHideSlide->setDirection(QPropertyAnimation::Backward);
      qobject_cast<QHBoxLayout*>(centralWidget()->layout())->insertWidget(0, cpWidgetSettings);
    }
    else {
      cpPropertyAnimationHideSlide->setDirection(QPropertyAnimation::Forward);
      centralWidget()->layout()->removeWidget(cpWidgetSettings);
      cpWidgetSettings->hide();
      // обратно поменять действие "Скрыть настройки" на "Показать настройки" с изменением иконки
      cpActionSettings->setText(tr("Показать настройки"));
      cpActionSettings->setIcon(*pPixmapMenu);
    }
  });
}
//-------------------------------------------------------------------------------------------------
void GUI::vSetCalc(QWidget* cpWidgetSettings, QAction* cpActionSaveFile, QAction* cpActionCalc,
                   QAction* cpActionPause, QAction* cpActionStop, QAction* cpActionReport,
                   QAction* cpActionLevel, QSpinBox* cpSpinBox)
{
  // лямбда расчета
  connect(cpActionCalc, &QAction::triggered, [this, cpWidgetSettings,
          cpActionSaveFile, cpActionCalc, cpActionPause, cpActionStop, cpActionReport,
          cpActionLevel, cpSpinBox]
  {
    if (!_bIsCalcRunning) {
      // включение флага о статусе расчета - расчет идет
      _bIsCalcRunning = true;

      // лямбда изменения доступа действий моделирования
      const auto vSetActionsAccess = [cpActionCalc, cpActionPause, cpActionStop] (bool b) {
        cpActionCalc->setEnabled(b);
        cpActionPause->setEnabled(!b);
        cpActionStop->setEnabled(!b);
      };

      // выключение всех настроек
      cpWidgetSettings->setEnabled(false);

      // включение действия сохранения расчета в файл
      cpActionSaveFile->setEnabled(true);

      // включение действия вывода отчета
      cpActionReport->setEnabled(false);

      // выключение действия "Рассчитать" и включение действий "Приостановить" и "Остановить" при
      // текущем расчете
      vSetActionsAccess(false);

      // добавление в строку состояния временный индикатор прогресса процесса
      const auto cpProgressBar = new QProgressBar(statusBar());
      statusBar()->addPermanentWidget(cpProgressBar, 1);

      // указатель на строитель графиков
      if (_pPlotterSession) {
        delete _pPlotterSession;
      }
      _pPlotterSession = new PlotterSession;

      // указатель на сессию расчета
      const auto cpMathModelSession = new TempDistSimulatorEngine::MathModelSession(this);

      // указатель на считанные входные данные
      _pInputData = getInputDataFromForm();

      // лямбда сохранения расчета в файл
      connect(cpActionSaveFile, &QAction::triggered, _pPlotterSession, [this, cpActionStop]
      {
        // окно вопроса сохранения при незавершенном расчете
        if (_bIsCalcRunning) {
          QMessageBox oMessageBoxSave;
          oMessageBoxSave.setIcon(QMessageBox::Question);
          oMessageBoxSave.setWindowTitle(tr("Запись в файл"));
          oMessageBoxSave.setText(tr("Расчет еще не закончен, вы уверены, что хотите сохранить "
                                     "файл? Ваш текущий расчет будет завершен."));
          oMessageBoxSave.addButton(tr("Да"), QMessageBox::YesRole);
          oMessageBoxSave.addButton(tr("Нет"), QMessageBox::NoRole);
          if (oMessageBoxSave.exec()) {
            return;
          }
          cpActionStop->activate(QAction::Trigger);
        }
        // считывание введенного названия файла
        QString oStrSaveFileName = QFileDialog::getSaveFileName(nullptr, tr("Сохранить файл"),
                                                                "MyResult.dat", "*.dat");
        // проверка правильности имени файла
        if (oStrSaveFileName.isEmpty()) {
          return;
        }
        if (!oStrSaveFileName.endsWith(".dat")) {
          QMessageBox::warning(nullptr, tr("Файл не сохранен"), tr("Неправильное имя файла."));
          return;
        }
        // ожидание завершения расчета
        if (_bIsCalcRunning) {
          _pPlotterSession->getPlotter()->vSetFinished(true);
          QEventLoop oEventLoopUntilMath;
          connect(this, &GUI::signalResumeRW, &oEventLoopUntilMath, &QEventLoop::quit);
          oEventLoopUntilMath.exec();
        }
        // сохранение в файл
        if (_pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim2D) {
          FileWriter(oStrSaveFileName, _pInputData, _pPlotterSession->getPlotter()->getTXY());
        }
        if (_pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
          FileWriter(oStrSaveFileName, _pInputData, _pPlotterSession->getPlotter()->getTXYZ());
        }
      });

      // настройка поля ввода выбора высотного уровня
      if (_pInputData->_enDimType == TempDistSimulatorEngine::EnDimType::enDim3D) {
        cpActionLevel->setVisible(true);
        cpSpinBox->setRange(0, _pInputData->_usNObjHeight);
        cpSpinBox->setValue(cpSpinBox->maximum());
      }
      else {
        cpActionLevel->setVisible(false);
      }

      // лямбда обновления состояния расчета в строке состояния и построения графика
      connect(cpMathModelSession,
              &TempDistSimulatorEngine::MathModelSession::signalIterationComplete,
              [this, cpProgressBar]
              (const int ciCompletePercent, const float fCurrentTime,
              const TempDistSimulatorEngine::ArrayDouble2D* cdTXY)
      {
        cpProgressBar->setFormat("%p% (" + QString::number(fCurrentTime, 'f', 2) + tr(" секунд)"));
        cpProgressBar->setValue(ciCompletePercent);
        if (ciCompletePercent == 100) {
          emit _pPlotterSession->getPlotter()->signalUpdatePlot(cdTXY);
        }
        else {
          emit _pPlotterSession->getPlotter()->signalUpdatePlot(cdTXY, true);
        }
      });

      // возобновление расчета после обновления графика
      connect(_pPlotterSession, &PlotterSession::signalUpdatePlotDone,
              cpMathModelSession, [cpMathModelSession]
      {
        emit cpMathModelSession->getMathModel()->signalResumePlotter();
      });

      // лямбда вывода окна предупреждения "Достигнута температура плавления"
      connect(cpMathModelSession,
              &TempDistSimulatorEngine::MathModelSession::signalTempFusionReach, []
      {
        QMessageBox::warning(nullptr, "", tr("Достигнута температура плавления материала,\n"
                                             "дальнейший расчет невозможен."));
      });

      // лямбда действия "Возобновить"
      connect(cpActionCalc, &QAction::triggered,
              cpMathModelSession, [cpMathModelSession, vSetActionsAccess]
      {
        cpMathModelSession->getMathModel()->vPause(false);
        emit cpMathModelSession->getMathModel()->signalResumeUser();
        vSetActionsAccess(false);
      });

      // лямбда приостановки рассчета (произойдет при завершении текущей итерации)
      connect(cpActionPause, &QAction::triggered,
              cpMathModelSession, [cpActionCalc, cpActionPause, cpMathModelSession]
      {
        cpActionCalc->setText(tr("Возобновить"));
        cpActionCalc->setEnabled(true);
        cpActionPause->setEnabled(false);
        cpMathModelSession->getMathModel()->vPause(true);
      });

      // лямбда немедленной остановки рассчета (произойдет при завершении текущей итерации)
      connect(cpActionStop, &QAction::triggered, cpMathModelSession,
              [this, cpActionCalc, cpActionPause, cpActionStop, cpMathModelSession]
      {
        _pPlotterSession->getPlotter()->vSetFinished(true);
        cpActionCalc->setEnabled(false);
        cpActionPause->setEnabled(false);
        cpActionStop->setEnabled(false);
        cpMathModelSession->getMathModel()->vStop();
        if (cpMathModelSession->getMathModel()->bIsPaused()) {
          cpMathModelSession->getMathModel()->vPause(false);
          emit cpMathModelSession->getMathModel()->signalResumeUser();
        }
      });

      // лямбда включения всех настроек, выключение флага о статусе расчета - расчет окончен,
      // включение действий моделирования и освобождение памяти при окончании текущего расчета
      const auto vFinishMathModel = [this, cpWidgetSettings, cpActionCalc, vSetActionsAccess,
                                     cpActionReport, cpMathModelSession, cpProgressBar]
      {
        // установка прошедшего времени
        _pInputData->_dTime = cpProgressBar->format().split('(').at(1).split(' ').at(0).toFloat();
        // виджет отчета по расчету
        const auto cpReportDataWidget = std::make_shared<ReportDataWidget>(_pInputData);
        cpActionReport->setEnabled(true);
        // лямбда отчета после завершения расчета
        connect(cpActionReport, &QAction::triggered, _pPlotterSession, [cpReportDataWidget]
        {
          cpReportDataWidget->exec();
        });
        _bIsCalcRunning = false;
        emit signalResumeRW();
        cpWidgetSettings->setEnabled(true);
        cpActionCalc->setText(tr("Рассчитать"));
        vSetActionsAccess(true);
        // ожидание завершения построения графика
        if (!_pPlotterSession->getPlotter()->bIsFinished()) {
          QEventLoop oEventLoopUntilPlot;
          connect(_pPlotterSession->getPlotter(), &Plotter::signalFinishPlot,
                  &oEventLoopUntilPlot, &QEventLoop::quit);
          oEventLoopUntilPlot.exec();
        }
        delete cpMathModelSession;
        delete cpProgressBar;
      };

      // лямбда записи трехмерного массива при окончании расчета
      connect(cpMathModelSession, &TempDistSimulatorEngine::MathModelSession::signalSendFinalArray,
              [this] (const TempDistSimulatorEngine::ArrayDouble3D* cdTXYZ)
      {
        emit _pPlotterSession->getPlotter()->signalWriteArray3D(cdTXYZ);
      });

      // лямбда действий после окончания расчета (двумерный случай)
      connect(cpMathModelSession, &TempDistSimulatorEngine::MathModelSession::signalFinished,
              [vFinishMathModel] { vFinishMathModel(); });

      // лямбда действий после окончания расчета и после перезаписи массива (трехмерный случай)
      connect(_pPlotterSession, &PlotterSession::signalFinishWriteArray3D,
              [vFinishMathModel] { vFinishMathModel(); });

      // добавление потока построения графика
      _pPlotterSession->vAddThread(_p3DSurface, _pInputData);
      // добавление потока расчета
      cpMathModelSession->vAddThread(_pInputData);
    }
  });
}
//-------------------------------------------------------------------------------------------------
void GUI::slotChangeHeightLevel(int iLevel)
{
  const auto cpMathModelSession = findChild<TempDistSimulatorEngine::MathModelSession*>();
  if (cpMathModelSession) {
    const auto cpMathModel3D =
      dynamic_cast<TempDistSimulatorEngine::MathModel3D*>(cpMathModelSession->getMathModel());
    if (cpMathModel3D) {
      cpMathModel3D->vChangeHeightLevel(iLevel);
      if (cpMathModel3D->bIsPaused()) {
        cpMathModel3D->vSendIterationResult(iLevel);
      }
    }
  }
  else {
    emit _pPlotterSession->getPlotter()->signalUpdateFinishedPlot(iLevel);
  }
}
//-------------------------------------------------------------------------------------------------
