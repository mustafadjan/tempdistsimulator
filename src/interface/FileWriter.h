#pragma once
//-------------------------------------------------------------------------------------------------
#include "Typedefs.h"
#include <memory>
class QJsonObject;
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
  struct InputData;
}
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс записи результатов расчета в файл
 */
class FileWriter final
{
public:

  /** @brief Конструктор для записи двумерного расчета */
  explicit FileWriter(const QString&, const std::shared_ptr<TempDistSimulatorEngine::InputData>,
                      const TempDistSimulatorEngine::ArrayDouble2D*);

  /** @brief Конструктор для записи тпехмерного расчета */
  explicit FileWriter(const QString&, const std::shared_ptr<TempDistSimulatorEngine::InputData>,
                      const TempDistSimulatorEngine::ArrayDouble3D*);

private:

  /** @brief Запись начальных параметров */
  void vInit(QJsonObject&, const std::shared_ptr<TempDistSimulatorEngine::InputData>) const;

  /** @brief Запись в файл */
  void vSaveToFile(const QString&, const QJsonObject&) const;

};
//-------------------------------------------------------------------------------------------------
