#include "PlotterSession.h"
#include "InputData.h"
#include "Plotter.h"
#include <QThread>
#include <Q3DSurface>
//-------------------------------------------------------------------------------------------------
PlotterSession::PlotterSession(QObject* pObject):
  QObject(pObject),
  _pPlotter(nullptr)
{
}
//-------------------------------------------------------------------------------------------------
PlotterSession::~PlotterSession()
{
  _pPlotter->thread()->exit();
  _pPlotter->thread()->wait();
  delete _pPlotter->thread();
  delete _pPlotter;
}
//-------------------------------------------------------------------------------------------------
void PlotterSession::vAddThread(QtDataVisualization::Q3DSurface* p3DSurface,
                                const std::shared_ptr<TempDistSimulatorEngine::InputData>
                                cpInputData)
{
  _pPlotter = new Plotter(p3DSurface, cpInputData);

  const auto cpThreadPlotter = new QThread;
  _pPlotter->moveToThread(cpThreadPlotter);

  // пересылка сигнала о завершении обновления данных на графике
  connect(p3DSurface->seriesList().last()->dataProxy(),
          &QtDataVisualization::QSurfaceDataProxy::arrayReset,
          this, &PlotterSession::signalUpdatePlotDone);

  // пересылка сигнала о завершении перезаписи трехмерного массива
  connect(_pPlotter, &Plotter::signalFinishWriteArray3D,
          this, &PlotterSession::signalFinishWriteArray3D);

  // старт процесса построения графика
  cpThreadPlotter->start();
}
//-------------------------------------------------------------------------------------------------
