#include "ReportDataWidget.h"
#include "InputData.h"
#include <QBoxLayout>
#include <QLabel>
//-------------------------------------------------------------------------------------------------
ReportDataWidget::ReportDataWidget(const std::shared_ptr<TempDistSimulatorEngine::InputData>
                                   cpInputData, QWidget* pParent):
  QDialog(pParent)
{
  // установка основных компонентов
  setWindowTitle(tr("Отчет расчета"));
  setLayout(new QVBoxLayout(this));
  // строки для размерности, типа теплового источника и материала
  QString oStrDim, oStrQw, oStrMaterialType;
  // определение строки для размерности
  switch (cpInputData->_enDimType) {
    case TempDistSimulatorEngine::EnDimType::enDim2D:
      oStrDim = tr("двумерный расчет");
      break;
    case TempDistSimulatorEngine::EnDimType::enDim3D:
      oStrDim = tr("трехмерный расчет");
  }
  // определение строки для типа теплового источника
  switch (cpInputData->_enQwType) {
    case TempDistSimulatorEngine::EnQwType::enQwDot:
      oStrQw = tr("точечный");
      break;
    case TempDistSimulatorEngine::EnQwType::enQwLinear:
      oStrQw = tr("линейный");
      break;
    case TempDistSimulatorEngine::EnQwType::enQwEllipse:
      oStrQw = tr("эллиптический");
  }
  // определение строки для материала
  switch (cpInputData->_enMaterialType) {
    case TempDistSimulatorEngine::EnMaterialType::enMaterialCopper:
      oStrMaterialType = tr("медь");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialSilver:
      oStrMaterialType = tr("серебро");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialGold:
      oStrMaterialType = tr("золото");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialZinc:
      oStrMaterialType = tr("цинк");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialAluminium:
      oStrMaterialType = tr("алюминий");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialTin:
      oStrMaterialType = tr("олово");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialLead:
      oStrMaterialType = tr("свинец");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialIron:
      oStrMaterialType = tr("железо");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialCobalt:
      oStrMaterialType = tr("кобальт");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialNickel:
      oStrMaterialType = tr("никель");
      break;
    case TempDistSimulatorEngine::EnMaterialType::enMaterialOther:
      oStrMaterialType = tr("другой материал");
  }
  // добавление всей информации на форму
  layout()->addWidget(new QLabel(tr("Размерность: ") + oStrDim, this));
  layout()->addWidget(new QLabel(tr("Тип теплового источника: ") + oStrQw, this));
  layout()->addWidget(new QLabel(tr("Количество шагов по ширине: ") + QString::number
                                 (cpInputData->_usNObjWidth), this));
  layout()->addWidget(new QLabel(tr("Количество шагов по длине: ") + QString::number
                                 (cpInputData->_usNObjLenght), this));
  if (oStrDim == "трехмерный расчет") {
    layout()->addWidget(new QLabel(tr("Количество шагов по высоте: ") + QString::number
                                   (cpInputData->_usNObjHeight), this));
  }
  layout()->addWidget(new QLabel(tr("Количество шагов по времени: ") + QString::number
                                 (cpInputData->_usNTime), this));
  layout()->addWidget(new QLabel(tr("Ширина, (м): ") + QString::number(cpInputData->_dObjWidth),
                                 this));
  layout()->addWidget(new QLabel(tr("Длина, (м): ") + QString::number(cpInputData->_dObjLenght),
                                 this));
  if (oStrDim == "трехмерный расчет") {
    layout()->addWidget(new QLabel(tr("Высота, (м): ") + QString::number(cpInputData->_dObjHeight),
                                   this));
  }
  layout()->addWidget(new QLabel(tr("Начальная температура объекта, (K): ") + QString::number
                                 (cpInputData->_dAreaTemp, 'f', 0), this));
  layout()->addWidget(new QLabel(tr("Температура окружающей среды, (K): ") + QString::number
                                 (cpInputData->_dObjTemp, 'f', 0), this));
  layout()->addWidget(new QLabel(tr("Время процесса, (с): ") +
                                 QString::number(cpInputData->_dTime), this));
  layout()->addWidget(new QLabel(tr("Удельная мощность источника\nтеплового излучения, (Вт/м^2): ")
                                 + QString::number(cpInputData->_dPowerDensity, 'f', 0), this));
  layout()->addWidget(new QLabel(tr("Материал: ") + oStrMaterialType, this));
  if (oStrMaterialType == "другой материал") {
    layout()->addWidget(new QLabel(tr("Плотность материала, (кг/м^3): ") + QString::number
                                   (cpInputData->_dObjDensity, 'f', 0), this));
    layout()->addWidget(new QLabel(tr("Удельная теплоемкость материала, (Дж/(кг*K)): ") +
                                   QString::number(cpInputData->_dObjHeatCapacity, 'f', 0), this));
    layout()->addWidget(new QLabel(tr("Теплопроводность материала, (Вт/(м*K)): ") + QString::number
                                   (cpInputData->_dObjThermalConductivity, 'f', 0), this));
    layout()->addWidget(new QLabel(tr("Температура плавления материала, (K): ") + QString::number
                                   (cpInputData->_dObjFusionTemp, 'f', 0), this));
  }
}
//-------------------------------------------------------------------------------------------------
