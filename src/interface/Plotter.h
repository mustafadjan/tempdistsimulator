#pragma once
//-------------------------------------------------------------------------------------------------
#include "Typedefs.h"
#include <memory>
#include <QObject>
//-------------------------------------------------------------------------------------------------
namespace QtDataVisualization {
  class Q3DSurface;
}
namespace TempDistSimulatorEngine {
  class InputData;
}
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс построения графика
 * @details Сигнальное решение обеспечивает более плавную работу интерфейса при работе в отдельном
 * потоке
 */
class Plotter final : public QObject
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit Plotter(QtDataVisualization::Q3DSurface*,
                   const std::shared_ptr<TempDistSimulatorEngine::InputData>,
                   QObject* pParent = nullptr);

  /** @brief Деструктор */
  ~Plotter();

  /** @brief Получить признак завершения построения графика */
  bool bIsFinished() const { return _bIsFinished; }

  /** @brief Установить признак завершения построения графика */
  void vSetFinished(bool b) { _bIsFinished = b; }

  /** @brief Получить конечную температуру двумерного объекта */
  const TempDistSimulatorEngine::ArrayDouble2D* getTXY();

  /** @brief Получить конечную температуру трехмерного объекта */
  const TempDistSimulatorEngine::ArrayDouble3D* getTXYZ() { return &_dTXYZ; }

  /** @brief Установить температуру элеменита двумерного объекта */
  void vSetTXY(int i, int j, double dValue) { _dTXY[i][j] = dValue; }

  /** @brief Установить температуру элеменита трехмерного объекта */
  void vSetTXYZ(int i, int j, int k, double dValue) { _dTXYZ[i][j][k] = dValue; }

private:

  /** @brief Признак завершения построения графика */
  bool _bIsFinished;

  /** @brief Указатель на область построения графиков */
  QtDataVisualization::Q3DSurface* _p3DSurface;

  /** @brief Конечная температура трехмерного объекта */
  TempDistSimulatorEngine::ArrayDouble3D _dTXYZ;

  /** @brief Высотный срез температуры трехмерного объекта (конечная температура двумерного объекта
   * при сохранении в файл) */
  TempDistSimulatorEngine::ArrayDouble2D _dTXY;

private slots:

  /** @brief Обновление данных на графике расчета */
  void slotUpdatePlot(const TempDistSimulatorEngine::ArrayDouble2D*, bool bIsFinished = true);

signals:

  /** @brief Обновление данных на графике расчета */
  void signalUpdatePlot(const TempDistSimulatorEngine::ArrayDouble2D*, bool bIsFinished = true)
  const;

  /** @brief Обновление данных на графике завершенного расчета (смена высотного уровня) */
  void signalUpdateFinishedPlot(const int) const;

  /** @brief Перезапись трехмерного массива при окончании расчета */
  void signalWriteArray3D(const TempDistSimulatorEngine::ArrayDouble3D*) const;

  /** @brief Завершение перезаписи трехмерного массива */
  void signalFinishWriteArray3D() const;

  /** @brief Завершение построения графика расчета */
  void signalFinishPlot() const;

};
//-------------------------------------------------------------------------------------------------
