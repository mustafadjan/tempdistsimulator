#include <QApplication>
#include <QSettings>
#include <QTranslator>
#include <QMessageBox>
#include "GUI.h"
//-------------------------------------------------------------------------------------------------
int main(int argc, char** argv)
{
  const QApplication coApplication(argc, argv);
  QCoreApplication::setOrganizationName("MAI");
  QCoreApplication::setApplicationName("TempDistSimulator");
  QCoreApplication::setApplicationVersion("ver. 1.0");

  // ночной режим
  #ifdef Q_OS_LINUX
  QSettings oSettings;
  oSettings.beginGroup("/Settings");
  bool bIsNightMode = (oSettings.value("NightMode", false).toBool());
  oSettings.endGroup();
  QPalette oPaletteNightMode;
  if (bIsNightMode) {
    oPaletteNightMode.setColor(QPalette::Window, QColor(53, 53, 53));
    oPaletteNightMode.setColor(QPalette::WindowText, Qt::white);
    oPaletteNightMode.setColor(QPalette::Base, QColor(15, 15, 15));
    oPaletteNightMode.setColor(QPalette::Text, Qt::white);
    oPaletteNightMode.setColor(QPalette::Button, QColor(53, 53, 53));
    oPaletteNightMode.setColor(QPalette::ButtonText, Qt::white);
    oPaletteNightMode.setColor(QPalette::Highlight, QColor(142, 45, 197).lighter());
    oPaletteNightMode.setColor(QPalette::HighlightedText, QColor(15, 15, 15));
    oPaletteNightMode.setColor(QPalette::Disabled, QPalette::WindowText, Qt::black);
    oPaletteNightMode.setColor(QPalette::Disabled, QPalette::Text, Qt::gray);
    QGuiApplication::setPalette(oPaletteNightMode);
  }
  #endif
  #ifdef Q_OS_WIN32
  bool bIsNightMode{false};
  #endif
  // перевод на английский язык при нерусской локали
  QTranslator oTranslatorEn;
  if (QLocale::system().name() != "ru_RU") {
    if (oTranslatorEn.load(QCoreApplication::applicationName() + QString("_en_US"))) {
      QCoreApplication::installTranslator(&oTranslatorEn);
    }
    else {
      QMessageBox::warning(nullptr, "Can't find English translate", "You will be switched on\n"
                                                                    "native language (Russian).");
    }
  }

  const GUI coGUI(bIsNightMode);

  return coApplication.exec();
}
//-------------------------------------------------------------------------------------------------
