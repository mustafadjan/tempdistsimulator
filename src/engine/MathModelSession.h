#pragma once
//-------------------------------------------------------------------------------------------------
#include "Typedefs.h"
#include <memory>
#include <QObject>
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
struct InputData;
class AbstractMathModel;
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс сессии математической модели
 * @details Расчет проходит в отдельном потоке
 */
class MathModelSession final : public QObject
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit MathModelSession(QObject* pParent = nullptr);

  /** @brief Деструктор */
  ~MathModelSession();

  /** @brief Добавление потока расчета */
  void vAddThread(const std::shared_ptr<InputData>);

  /** @brief Получить указатель математической модели */
  AbstractMathModel* getMathModel() { return _pMathModel; }

private:

  /** @brief Указатель математической модели */
  AbstractMathModel* _pMathModel;

signals:

  /** @brief Завершение итерации расчета */
  void signalIterationComplete(const int, const float, const ArrayDouble2D*) const;

  /** @brief Достигнута темпелатура плавления */
  void signalTempFusionReach() const;

  /** @brief Послать рассчитаный трехмерный массив после окончания расчета */
  void signalSendFinalArray(const ArrayDouble3D*) const;

  /** @brief Завершение потока расчета */
  void signalFinished() const;

};
//-------------------------------------------------------------------------------------------------
}
