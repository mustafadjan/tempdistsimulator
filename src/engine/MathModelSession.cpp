#include "MathModelSession.h"
#include "InputData.h"
#include "MathModel2D.h"
#include "MathModel3D.h"
#include <QThread>
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
MathModelSession::MathModelSession(QObject* pObject):
  QObject(pObject),
  _pMathModel(nullptr)
{
}
//-------------------------------------------------------------------------------------------------
MathModelSession::~MathModelSession()
{
  _pMathModel->vStop();
  _pMathModel->thread()->exit();
  _pMathModel->thread()->wait();
  delete _pMathModel->thread();
  delete _pMathModel;
}
//-------------------------------------------------------------------------------------------------
void MathModelSession::vAddThread(const std::shared_ptr<InputData> cpInputData)
{
  switch (cpInputData->_enDimType) {
    case EnDimType::enDim2D:
      _pMathModel = new MathModel2D(cpInputData);
      break;
    case EnDimType::enDim3D:
      _pMathModel = new MathModel3D(cpInputData);
  }

  if (_pMathModel) {
    const auto cpThreadMathModel = new QThread;
    _pMathModel->moveToThread(cpThreadMathModel);

    // старт процесса расчета в отдельном потоке
    connect(cpThreadMathModel, &QThread::started, _pMathModel, &AbstractMathModel::slotProcess);

    // пересылка сигнала о завершении итерации расчета
    connect(_pMathModel, &AbstractMathModel::signalIterationComplete,
            this, &MathModelSession::signalIterationComplete);

    // пересылка сигнала о достижении температуры плавления
    connect(_pMathModel, &AbstractMathModel::signalTempFusionReach,
            this, &MathModelSession::signalTempFusionReach);

    // указатель двумерной математической модели
    const auto cpMathModel2D = dynamic_cast<MathModel2D*>(_pMathModel);
    if (cpMathModel2D) {
      // пересылка сигнала о завершении расчета
      connect(cpMathModel2D, &MathModel2D::signalFinished, this,&MathModelSession::signalFinished);
    }

    // указатель трехмерной математической модели
    const auto cpMathModel3D = dynamic_cast<MathModel3D*>(_pMathModel);
    if (cpMathModel3D) {
      // пересылка сигнала рассчитаного массива
      connect(cpMathModel3D, &MathModel3D::signalSendFinalArray,
              this, &MathModelSession::signalSendFinalArray);
    }

    // старт процесса расчета
    cpThreadMathModel->start();
  }
}
//-------------------------------------------------------------------------------------------------
}
