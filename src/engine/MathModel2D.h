#pragma once
//-------------------------------------------------------------------------------------------------
#include "AbstractMathModel.h"
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
struct InputData;
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс двумерной математической модели
 * @details Позволяет рассчитать распределение температур в объекте по входным данным
 */
class MathModel2D final : public AbstractMathModel
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit MathModel2D(const std::shared_ptr<InputData>, QObject* pParent = nullptr);

  /** @brief Деструктор */
  ~MathModel2D();

private:

  /** @brief Срез температур для расчета прогоночных коэффициентов */
  double *_dTY1;

  /** @brief Текущая температура объекта */
  ArrayDouble2D _dTXY;

  /** @brief Бэкап последней итерации текущей температуры объекта */
  ArrayDouble2D _dTXYbu;

public slots:

  /** @brief Запуск расчета */
  void slotProcess() override final;

signals:

  /** @brief Завершение расчета */
  void signalFinished() const;

};
//-------------------------------------------------------------------------------------------------
}
