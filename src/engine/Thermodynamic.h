#pragma once
#include <memory>
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
class InputData;
//-------------------------------------------------------------------------------------------------
/** @brief Расчет плотности материала */
double dDensityCalc(const std::shared_ptr<InputData>, const double);

/** @brief Расчет удельной теплоемкости материала */
double dHeatCapacityCalc(const std::shared_ptr<InputData>, const double);

/** @brief Расчет теплопроводности материала */
double dThermalConductivityCalc(const std::shared_ptr<InputData>, const double);
//-------------------------------------------------------------------------------------------------
}
