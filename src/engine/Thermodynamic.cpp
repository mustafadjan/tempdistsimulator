#include "Thermodynamic.h"
#include "InputData.h"
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
double dDensityCalc(const std::shared_ptr<InputData> cpInputData, const double cdT)
{
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialCopper) {
    if (cdT <= 300.) {
      return 8933.;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 9122. - (.63 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 9038. - (.42 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 9073. - (.49 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 9085. - (.51 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 9232. - (.72 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 8928. - (.34 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 9117. - (.55 * cdT);
    }
    if (cdT > 1000. && cdT <= 1200.) {
      return 9147. - (.58 * cdT);
    }
    if (cdT > 1200. && cdT <= 1300.) {
      return 9135. - (.57 * cdT);
    }
    if (cdT > 1300. && cdT <= 1357.6) {
      return 8394. - (55. * (cdT - 1300.) / 96.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialSilver) {
    if (cdT <= 200.) {
      return 10540.;
    }
    if (cdT > 200. && cdT <= 300.) {
      return 10634. - (.47 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 10682. - (.63 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 10670. - (.6 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 10720. - (.7 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 10480. - (.3 * cdT);
    }
    if (cdT > 700. && cdT <= 900.) {
      return 10760. - (.7 * cdT);
    }
    if (cdT > 900. && cdT <= 1200.) {
      return 10850. - (.8 * cdT);
    }
    if (cdT > 1200. && cdT <= 1235.08) {
      return 9890. - (750. * (cdT - 1200.) / 877.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialGold) {
    if (cdT <= 300.) {
      return 19300.;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 19540. - (.8 * cdT);
    }
    if (cdT > 400. && cdT <= 700.) {
      return 19580. - (.9 * cdT);
    }
    if (cdT > 700. && cdT <= 1000.) {
      return 19650. - cdT;
    }
    if (cdT > 1000. && cdT <= 1300.) {
      return 19750. - (1.1 * cdT);
    }
    if (cdT > 1300. && cdT <= 1337.58) {
      return 18320. - (2500. * (cdT - 1300.) / 1879.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialZinc) {
    if (cdT <= 100.) {
      return 7260.;
    }
    if (cdT > 100. && cdT <= 200.) {
      return 7330. - (.7 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 7310. - (.6 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 7340. - (.7 * cdT);
    }
    if (cdT > 400. && cdT <= 600.) {
      return 7300. - (.6 * cdT);
    }
    if (cdT > 600. && cdT <= 692.73) {
      return 6940. - (2000. * (cdT - 600.) / 9273.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialAluminium) {
    if (cdT <= 100.) {
      return 2725.;
    }
    if (cdT > 100. && cdT <= 200.) {
      return 2735. - (cdT / 10.);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 2751. - (.18 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 2763. - (.22 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 2715. - (cdT / 10.);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 2730. - (.13 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 2808. - (.26 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 2843. - (.31 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 2875. - (.35 * cdT);
    }
    if (cdT > 900. && cdT <= 933.61) {
      return 2560. - ((cdT - 900.) / 3.361);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialTin) {
    if (cdT <= 200.) {
      return 7344.;
    }
    if (cdT > 200. && cdT <= 300.) {
      return 7444. - (cdT / 2.);
    }
    if (cdT > 300. && cdT <= 500.) {
      return 7456. - (.54 * cdT);
    }
    if (cdT > 500. && cdT <= 505.12) {
      return 7186. - (25. * (cdT - 500.) / 64.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialLead) {
    if (cdT <= 100.) {
      return 11531.;
    }
    if (cdT > 100. && cdT <= 200.) {
      return 11627. - (.96 * cdT);
    }
    if (cdT > 200. && cdT <= 400.) {
      return 11625. - (.95 * cdT);
    }
    if (cdT > 400. && cdT <= 600.) {
      return 11617. - (.93 * cdT);
    }
    if (cdT > 600. && cdT <= 600.652) {
      return 11059. - ((cdT - 600.) / .652);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialIron) {
    if (cdT <= 300.) {
      return 7870.;
    }
    if (cdT > 300. && cdT <= 500.) {
      return 7960. - (.3 * cdT);
    }
    if (cdT > 500. && cdT <= 700.) {
      return 8010. - (.4 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 7940. - (.3 * cdT);
    }
    if (cdT > 800. && cdT <= 1000.) {
      return 8020. - (.4 * cdT);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 7920. - (.3 * cdT);
    }
    if (cdT > 1100. && cdT <= 1810.) {
      return 7590. - (55. * (cdT - 1100.) / 71.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialCobalt) {
    if (cdT <= 300.) {
      return 8830.;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 8950. - (.4 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 8990. - (cdT / 2.);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 8740.;
    }
    if (cdT > 600. && cdT <= 700.) {
      return 9220. - (.8 * cdT);
    }
    if (cdT > 700. && cdT <= 1767.) {
      return 8660. - (830. * (cdT - 700.) / 1067.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialNickel) {
    if (cdT <= 300.) {
      return 8900.;
    }
    if (cdT > 300. && cdT <= 700.) {
      return 9020. - (.4 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 9090. - (cdT / 2.);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 9010. - (.4 * cdT);
    }
    if (cdT > 900. && cdT <= 1600.) {
      return 9100. - (cdT / 2.);
    }
    if (cdT > 1600. && cdT <= 1728.) {
      return 10800. - (25. * cdT / 16.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialOther) {
    if (cdT <= cpInputData->_dObjFusionTemp) {
      return cpInputData->_dObjDensity;
    }
  }
  return 0.;
}
//-------------------------------------------------------------------------------------------------
double dHeatCapacityCalc(const std::shared_ptr<InputData> cpInputData, const double cdT)
{
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialCopper) {
    if (cdT <= 300.) {
      return 385.;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 346.9 + (.127 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 356.5 + (.103 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 363.5 + (.089 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 367.7 + (.082 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 370.5 + (.078 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 362.5 + (.088 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 354.4 + (.097 * cdT);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 322.4 + (.129 * cdT);
    }
    if (cdT > 1100. && cdT <= 1200.) {
      return 282.8 + (.165 * cdT);
    }
    if (cdT > 1200. && cdT <= 1300.) {
      return 172.4 + (.257 * cdT);
    }
    if (cdT > 1300. && cdT <= 1357.6) {
      return 506.5 + (187. * (cdT - 1300.) / 576.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialSilver) {
    if (cdT <= 300.) {
      return 235.4;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 224. + (.038 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 220.4 + (.047 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 214.9 + (.058 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 214.3 + (.059 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 210.1 + (.065 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 206.1 + (.07 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 202.5 + (.074 * cdT);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 199.5 + (.077 * cdT);
    }
    if (cdT > 1100. && cdT <= 1200.) {
      return 195.1 + (.081 * cdT);
    }
    if (cdT > 1200. && cdT <= 1235.08) {
      return 292.3 + (235. * (cdT - 1200.) / 1754.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialGold) {
    if (cdT <= 300.) {
      return 128.7;
    }
    if (cdT > 300. && cdT <= 800.) {
      return 122.1 + (.022 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 120.5 + (.024 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 115.1 + (.03 * cdT);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 102.1 + (.043 * cdT);
    }
    if (cdT > 1100. && cdT <= 1200.) {
      return 84.5 + (.059 * cdT);
    }
    if (cdT > 1200. && cdT <= 1300.) {
      return 58.1 + (.081 * cdT);
    }
    if (cdT > 1300. && cdT <= 1337.58) {
      return 163.4 + (305. * (cdT - 1300.) / 1879.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialZinc) {
    if (cdT <= 300.) {
      return 389.;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 348.2 + (.136 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 342.6 + (.15 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 325.1 + (.185 * cdT);
    }
    if (cdT > 600. && cdT <= 692.73) {
      return 436.1 + (1660. * (cdT - 600.) / 9273.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialAluminium) {
    if (cdT <= 100.) {
      return 483.6;
    }
    if (cdT > 100. && cdT <= 200.) {
      return 167. + (3.166 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 593.2 + (1.035 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 760.9 + (.476 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 789.3 + (.405 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 767.3 + (.449 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 715.7 + (.535 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 645. + (.636 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 558.6 + (.744 * cdT);
    }
    if (cdT > 900. && cdT <= 933.61) {
      return 1228.2 + (2760. * (cdT - 900.) / 3361.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialTin) {
    if (cdT <= 100.) {
      return 187.1;
    }
    if (cdT > 100. && cdT <= 200.) {
      return 159.4 + (.277 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 187. + (.139 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 184.3 + (.148 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 171.5 + (.18 * cdT);
    }
    if (cdT > 500. && cdT <= 505.12) {
      return 261.5 + ((cdT - 500.) / 5.12);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialLead) {
    if (cdT <= 50.) {
      return 103.;
    }
    if (cdT > 50. && cdT <= 100.) {
      return 89.2 + (.276 * cdT);
    }
    if (cdT > 100. && cdT <= 200.) {
      return 110.4 + (.064 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 114.6 + (.043 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 111.6 + (.053 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 113.6 + (.048 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 115.1 + (.045 * cdT);
    }
    if (cdT > 600. && cdT <= 600.652) {
      return 141.1 + ((cdT - 600.) / 6.52);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialIron) {
    if (cdT <= 100.) {
      return 213.1;
    }
    if (cdT > 100. && cdT <= 200.) {
      return 44.2 + (1.689 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 252. + (.65 * cdT);
    }
    if (cdT > 300. && cdT <= 500.) {
      return 321. + (.42 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 326. + (.41 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 296. + (.46 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 198. + (.6 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return (.92 * cdT) - 58.;
    }
    if (cdT > 900. && cdT <= 1000.) {
      return (2.64 * cdT) - 1606.;
    }
    if (cdT > 1000. && cdT <= 1042.) {
      return 1034. + (101. * (cdT - 1000.) / 21.);
    }
    if (cdT > 1042. && cdT <= 1100.) {
      return 1236. - (407. * (cdT - 1042.) / 58.);
    }
    if (cdT > 1100. && cdT <= 1183.) {
      return 829. - (87. * (cdT - 1100.) / 83.);
    }
    if (cdT > 1183. && cdT <= 1200.) {
      return 607. + ((cdT - 1183.) / 17.);
    }
    if (cdT > 1200. && cdT <= 1400.) {
      return 428. + (.15 * cdT);
    }
    if (cdT > 1400. && cdT <= 1600.) {
      return 435. + (.145 * cdT);
    }
    if (cdT > 1600. && cdT <= 1667.) {
      return 667. + (12. * (cdT - 1600.) / 67.);
    }
    if (cdT > 1667. && cdT <= 1800.) {
      return 737. + (23. * (cdT - 1667.) / 133.);
    }
    if (cdT > 1800. && cdT <= 1810.) {
      return 400. + (cdT / 5.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialCobalt) {
    if (cdT <= 300.) {
      return 421.3;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 333.7 + (.292 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 336.9 + (.284 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 354.9 + (.248 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 362.7 + (.235 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 301. + (.312 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 265.8 + (.356 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 210. + (.418 * cdT);
    }
    if (cdT > 1000. && cdT <= 1200.) {
      return 98. + (.53 * cdT);
    }
    if (cdT > 1200. && cdT <= 1394.) {
      return 734. + (143. * (cdT - 1200.) / 97.);
    }
    if (cdT > 1394. && cdT <= 1400.) {
      return 1020. - (110. * (cdT - 1394.) / 3.);
    }
    if (cdT > 1400. && cdT <= 1600.) {
      return 1850. - (.75 * cdT);
    }
    if (cdT > 1600. && cdT <= 1767.) {
      return 650. - (8. * (cdT - 1600.) / 167.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialNickel) {
    if (cdT <= 300.) {
      return 443.6;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 322.1 + (.405 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 324.5 + (.399 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 189. + (.67 * cdT);
    }
    if (cdT > 600. && cdT <= 629.6) {
      return 591. + (395. * (cdT - 600.) / 148.);
    }
    if (cdT > 629.6 && cdT <= 700.) {
      return 670. - (365. * (cdT - 629.6) / 176.);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 489. + (cdT / 20.);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 417. + (.14 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 372. + (.19 * cdT);
    }
    if (cdT > 1000. && cdT <= 1200.) {
      return 407. + (.155 * cdT);
    }
    if (cdT > 1200. && cdT <= 1400.) {
      return 497. + (.08 * cdT);
    }
    if (cdT > 1400. && cdT <= 1600.) {
      return 553. + (cdT / 25.);
    }
    if (cdT > 1600. && cdT <= 1728.) {
      return 517. + (cdT / 16.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialOther) {
    if (cdT <= cpInputData->_dObjFusionTemp) {
      return cpInputData->_dObjHeatCapacity;
    }
  }
  return 0.;
}
//-------------------------------------------------------------------------------------------------
double dThermalConductivityCalc(const std::shared_ptr<InputData> cpInputData, const double cdT)
{
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialCopper) {
    if (cdT <= 50.) {
      return 1250.;
    }
    if (cdT > 50. && cdT <= 100.) {
      return 2018. - (15.36 * cdT);
    }
    if (cdT > 100. && cdT <= 200.) {
      return 551. - (.69 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 435.2 - (.111 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 433.1 - (.104 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 415.9 - (.061 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 427.9 - (.085 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 420.1 - (.072 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 432. - (.089 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 404.8 - (.055 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 410.2 - (.061 * cdT);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 465.2 - (.116 * cdT);
    }
    if (cdT > 1100. && cdT <= 1200.) {
      return 448.7 - (.101 * cdT);
    }
    if (cdT > 1200. && cdT <= 1300.) {
      return 392.3 - (.054 * cdT);
    }
    if (cdT > 1300. && cdT <= 1357.6) {
      return 322.1 - (17. * (cdT - 1300.) / 192.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialSilver) {
    if (cdT <= 200.) {
      return 430.;
    }
    if (cdT > 200. && cdT <= 300.) {
      return 431. - (cdT / 200.);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 445.7 - (.054 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 446.1 - (.055 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 441.6 - (.046 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 456.6 - (.071 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 467.1 - (.086 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 466.3 - (.085 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 471.7 - (.091 * cdT);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 491.7 - (.111 * cdT);
    }
    if (cdT > 1100. && cdT <= 1200.) {
      return 491.7 - (.111 * cdT);
    }
    if (cdT > 1200. && cdT <= 1235.08) {
      return 358.5 - (175. * (cdT - 1200.) / 1754.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialGold) {
    if (cdT <= 200.) {
      return 322.7;
    }
    if (cdT > 200. && cdT <= 300.) {
      return 332.7 - (cdT / 20.);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 342.3 - (.082 * cdT);
    }
    if (cdT > 400. && cdT <= 600.) {
      return 335.9 - (.066 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 338.9 - (.071 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 341. - (.074 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 353. - (.089 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 317.9 - (cdT / 20.);
    }
    if (cdT > 1000. && cdT <= 1100.) {
      return 315.9 - (.048 * cdT);
    }
    if (cdT > 1100. && cdT <= 1200.) {
      return 289.5 - (.024 * cdT);
    }
    if (cdT > 1200. && cdT <= 1300.) {
      return 305.1 - (.037 * cdT);
    }
    if (cdT > 1300. && cdT <= 1337.58) {
      return 257.;
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialZinc) {
    if (cdT <= 300.) {
      return 115.;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 130. - (cdT / 20.);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 118. - (cdT / 50.);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 133. - (cdT / 20.);
    }
    if (cdT > 600. && cdT <= 692.73) {
      return 103. - ((cdT - 600.) / 30.91);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialAluminium) {
    if (cdT <= 50.) {
      return 1350.;
    }
    if (cdT > 50. && cdT <= 100.) {
      return 2399.6 - (20.992 * cdT);
    }
    if (cdT > 100. && cdT <= 200.) {
      return 364. - (.636 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 238.6 - (.009 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 238. - (.007 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 237.2 - (cdT / 200.);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 257.7 - (.046 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 264.3 - (.057 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 252.4 - (.04 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 242.8 - (.028 * cdT);
    }
    if (cdT > 900. && cdT <= 933.61) {
      return 217.6 + ((cdT - 900.) / 336.1);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialTin) {
    if (cdT <= 50.) {
      return 115.;
    }
    if (cdT > 50. && cdT <= 100.) {
      return 144.8 - (.596 * cdT);
    }
    if (cdT > 100. && cdT <= 200.) {
      return 97.1 - (.119 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 86.7 - (.067 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 78.6 - (cdT / 25.);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 71.8 - (.023 * cdT);
    }
    if (cdT > 500. && cdT <= 505.12) {
      return 60.3;
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialLead) {
    if (cdT <= 50.) {
      return 43.2;
    }
    if (cdT > 50. && cdT <= 100.) {
      return 47.2 - (.08 * cdT);
    }
    if (cdT > 100. && cdT <= 200.) {
      return 41.9 - (.027 * cdT);
    }
    if (cdT > 200. && cdT <= 300.) {
      return 39.3 - (.014 * cdT);
    }
    if (cdT > 300. && cdT <= 400.) {
      return 38.1 - (cdT / 100.);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 38.9 - (.012 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 39.4 - (.013 * cdT);
    }
    if (cdT > 600. && cdT <= 600.652) {
      return 31.6;
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialIron) {
    if (cdT <= 300.) {
      return 79.9;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 111.4 - (.105 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 99.8 - (.076 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 95.3 - (.067 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 93.5 - (.064 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 90.7 - (.06 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 86.7 - (.055 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 84. - (.052 * cdT);
    }
    if (cdT > 1000. && cdT <= 1042.) {
      return 32. - (11. * (cdT - 1000.) / 70.);
    }
    if (cdT > 1042. && cdT <= 1100.) {
      return 25.4 + (12. * (cdT - 1042.) / 145.);
    }
    if (cdT > 1100. && cdT <= 1183.) {
      return 30.2 - ((cdT - 1100.) / 415.);
    }
    if (cdT > 1183. && cdT <= 1200.) {
      return 29.;
    }
    if (cdT > 1200. && cdT <= 1600.) {
      return 14. + (cdT / 80.);
    }
    if (cdT > 1600. && cdT <= 1667.) {
      return 34.;
    }
    if (cdT > 1667. && cdT <= 1810.) {
      return 35.;
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialCobalt) {
    if (cdT <= 300.) {
      return 94.9;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 130. - (.117 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 119.6 - (.091 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 105.1 - (.062 * cdT);
    }
    if (cdT > 600. && cdT <= 700.) {
      return 105.7 - (.063 * cdT);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 97.4 - (.053 * cdT);
    }
    if (cdT > 800. && cdT <= 1000.) {
      return 71. - (cdT / 50.);
    }
    if (cdT > 1000. && cdT <= 1200.) {
      return 51.;
    }
    if (cdT > 1200. && cdT <= 1394.) {
      return 51. - (9. * (cdT - 1200.) / 194.);
    }
    if (cdT > 1394. && cdT <= 1400.) {
      return 42.;
    }
    if (cdT > 1400. && cdT <= 1600.) {
      return 28. + (cdT / 100.);
    }
    if (cdT > 1600. && cdT <= 1767.) {
      return 44. + (7. * (cdT - 1600.) / 167.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialNickel) {
    if (cdT <= 300.) {
      return 90.4;
    }
    if (cdT > 300. && cdT <= 400.) {
      return 122.5 - (.107 * cdT);
    }
    if (cdT > 400. && cdT <= 500.) {
      return 110.1 - (.076 * cdT);
    }
    if (cdT > 500. && cdT <= 600.) {
      return 116.1 - (.088 * cdT);
    }
    if (cdT > 600. && cdT <= 629.6) {
      return 63.3 - (51. * (cdT - 600.) / 296.);
    }
    if (cdT > 629.6 && cdT <= 700.) {
      return 58.2 + (27. * (cdT - 629.6) / 704.);
    }
    if (cdT > 700. && cdT <= 800.) {
      return 37.1 + (.034 * cdT);
    }
    if (cdT > 800. && cdT <= 900.) {
      return 49.1 + (.019 * cdT);
    }
    if (cdT > 900. && cdT <= 1000.) {
      return 18.5 + (.053 * cdT);
    }
    if (cdT > 1000. && cdT <= 1200.) {
      return 51. + (.0205 * cdT);
    }
    if (cdT > 1200. && cdT <= 1400.) {
      return 69. + (.0055 * cdT);
    }
    if (cdT > 1400. && cdT <= 1600.) {
      return 72.5 + (.003 * cdT);
    }
    if (cdT > 1600. && cdT <= 1728.) {
      return 77.3 + (3. * (cdT - 1600.) / 320.);
    }
  }
  if (cpInputData->_enMaterialType == EnMaterialType::enMaterialOther) {
    if (cdT <= cpInputData->_dObjFusionTemp) {
      return cpInputData->_dObjThermalConductivity;
    }
  }
  return 0.;
}
//-------------------------------------------------------------------------------------------------
}
