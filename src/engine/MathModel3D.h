#pragma once
//-------------------------------------------------------------------------------------------------
#include "AbstractMathModel.h"
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
struct InputData;
//-------------------------------------------------------------------------------------------------
/**
 * @brief Класс трехмерной математической модели
 * @details Позволяет рассчитать распределение температур в объекте по входным данным
 */
class MathModel3D final : public AbstractMathModel
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit MathModel3D(const std::shared_ptr<InputData>, QObject* pParent = nullptr);

  /** @brief Деструктор */
  ~MathModel3D();

  /** @brief Изменение высотного уровня */
  void vChangeHeightLevel(const int ciLevel) { _usK = static_cast<quint16>(ciLevel); }

  /** @brief Послать результат расчета итерации */
  void vSendIterationResult(const int);

private:

  /** @brief Шаг по высоте объекта */
  const double _cdZStep;

  /** @brief Срезы температур для расчета прогоночных коэффициентов */
  double *_dTY1, *_dTZ1;

  /** @brief Срез температур для расчета термодинамических функций */
  double *_dTYbu;

  /** @brief Срез температур для записи результатов трехточечной прогонки */
  double *_dTZ;

  /** @brief Коэффициенты для расчета температур */
  double *_dAzk, *_dBzk, *_dCzk, *_dDzk;

  /** @brief Высотный уровень */
  quint16 _usK;

  /** @brief Текущая температура объекта */
  ArrayDouble3D _dTXYZ;

  /** @brief Бэкап последней итерации текущей температуры объекта */
  ArrayDouble3D _dTXYZbu;

  /** @brief Высотный срез температуры объекта */
  ArrayDouble2D _dTXY;

public slots:

  /** @brief Запуск расчета */
  void slotProcess() override final;

signals:

  /** @brief Послать рассчитаный массив после окончания расчета */
  void signalSendFinalArray(const ArrayDouble3D*) const;

};
//-------------------------------------------------------------------------------------------------
}
