#include "AbstractMathModel.h"
#include "InputData.h"
#include "Thermodynamic.h"
#include <qmath.h>
#include <QEventLoop>
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
AbstractMathModel::AbstractMathModel(const std::shared_ptr<InputData> cpInputData, QObject* pParent):
  QObject(pParent),
  _cpInputData     (cpInputData),
  _dQXY            (new double*[cpInputData->_usNObjWidth + 1]),
  _cdXStep         (cpInputData->_dObjWidth  / static_cast<double>(cpInputData->_usNObjWidth)),
  _cdYStep         (cpInputData->_dObjLenght / static_cast<double>(cpInputData->_usNObjLenght)),
  _cdTau           (cpInputData->_dTime      / static_cast<double>(cpInputData->_usNTime)),
  _dTX1            (new double[cpInputData->_usNObjLenght - 1]),
  _dTXbu           (new double[cpInputData->_usNObjLenght - 1]),
  _dTX             (new double[cpInputData->_usNObjWidth + 1]),
  _dTY             (new double[cpInputData->_usNObjLenght + 1]),
  _dQ1             (new double[cpInputData->_usNObjWidth - 1]),
  _dAxi            (new double[cpInputData->_usNObjWidth + 1]),
  _dBxi            (new double[cpInputData->_usNObjWidth + 1]),
  _dCxi            (new double[cpInputData->_usNObjWidth + 1]),
  _dDxi            (new double[cpInputData->_usNObjWidth + 1]),
  _dAyj            (new double[cpInputData->_usNObjLenght + 1]),
  _dByj            (new double[cpInputData->_usNObjLenght + 1]),
  _dCyj            (new double[cpInputData->_usNObjLenght + 1]),
  _dDyj            (new double[cpInputData->_usNObjLenght + 1]),
  _bPause          (false),
  _bStop           (false),
  _iPercentComplite(0),
  _fCurrentTime    (0.f)
{
  // создание поля мощностей
  for (quint16 i = 0; i < cpInputData->_usNObjWidth + 1; ++i) {
    _dQXY[i] = new double[cpInputData->_usNObjLenght + 1];
    // обнуление поля мощностей
    for (quint16 j = 0; j < cpInputData->_usNObjLenght + 1; ++j) {
      _dQXY[i][j] = 0.;
    }
  }
  /// расчет поля удельной мощности источника теплового излучения (для разностной сетки взят
  /// примерно, требует уточнения)
  // обновление поля мощности: точка с заданной мощностью в центре
  if (cpInputData->_enQwType == EnQwType::enQwDot) {
    _dQXY[cpInputData->_usNObjWidth / 2][cpInputData->_usNObjLenght / 2] =
      cpInputData->_dPowerDensity * (cpInputData->_usNObjWidth + 1) *
      (cpInputData->_usNObjLenght + 1);
  }
  // обновление поля мощности: линия с заданной мощностью в каждой точке в центре по ширине
  // (от 0.25x до 0.75x, т.е. половина по ширине)
  if (cpInputData->_enQwType == EnQwType::enQwLinear) {
    for (quint16 i = (cpInputData->_usNObjWidth / 4) + 1; i <= 3*cpInputData->_usNObjWidth/4;++i) {
      _dQXY[i][cpInputData->_usNObjLenght / 2] = cpInputData->_dPowerDensity *
        (cpInputData->_usNObjWidth + 1) * (cpInputData->_usNObjLenght + 1) /
        (cpInputData->_usNObjWidth / 2.);
    }
  }
  // обновление поля мощности: эллипс с распределенной мощностью (макс. в центре) согласно
  // распределению значения z в полуэллипсоиде, если его основание лежит в плоскости z
  // (z = _dObjHeight)
  // ((x - x0)^2 / a^2) + ((y - y0)^2 / b^2) + (z / c)^2
  if (cpInputData->_enQwType == EnQwType::enQwEllipse) {
    for (quint16 i = 0; i <= cpInputData->_usNObjWidth; ++i) {
      for (quint16 j = 0; j <= cpInputData->_usNObjLenght; ++j) {
        double dMidZValue = 1. - qPow(((_cdXStep * static_cast<double>(i)) -
                            (cpInputData->_dObjWidth / 2.)) / (cpInputData->_dObjWidth / 4.), 2.)
                            - qPow(((_cdYStep * static_cast<double>(j)) -
                            (cpInputData->_dObjLenght / 2.)) / (cpInputData->_dObjLenght / 4.),2.);
        if (dMidZValue > 0) {
          _dQXY[i][j] = qSqrt(qPow(cpInputData->_dPowerDensity, 2.) * dMidZValue) / M_PI_4;
        }
      }
    }
  }
}
//-------------------------------------------------------------------------------------------------
AbstractMathModel::~AbstractMathModel()
{
  for (quint16 i = 0; i < _cpInputData->_usNObjWidth + 1; ++i) {
    delete [] _dQXY[i];
  }
  delete [] _dQXY;
  delete [] _dTX1;
  delete [] _dTXbu;
  delete [] _dTX;
  delete [] _dTY;
  delete [] _dQ1;
  delete [] _dAxi;
  delete [] _dBxi;
  delete [] _dCxi;
  delete [] _dDxi;
  delete [] _dAyj;
  delete [] _dByj;
  delete [] _dCyj;
  delete [] _dDyj;
}
//-------------------------------------------------------------------------------------------------
void AbstractMathModel::vABCD(const quint16 cusNObjSize, const double cdT0, const double cdTn,
                              double* dA, double* dB, double* dC, double* dD, const double* cdT1,
                              const double* cdTbu, const double* cdQ1, const double cdStep,
                              const double cdTau, const double cdDimCoef) const
{
  double dMidValue  = 5.6 * cdStep / dThermalConductivityCalc(_cpInputData, cdT0);
  double dMidValue2 = 5.6 * cdStep / dThermalConductivityCalc(_cpInputData, cdTn);
  // расчет граничных условий
  dB[0] = dC[cusNObjSize] = 1.;
  dA[0] = 1. + dMidValue;
  dD[0] = _cpInputData->_dAreaTemp * dMidValue;
  dA[cusNObjSize] = 1. + dMidValue2;
  dD[cusNObjSize] = _cpInputData->_dAreaTemp * dMidValue2;
  for (quint16 n = 1; n < cusNObjSize; ++n) {
    dMidValue = 2. * cdDimCoef * dHeatCapacityCalc(_cpInputData, cdTbu[n - 1]) *
                dDensityCalc(_cpInputData, cdTbu[n - 1]) / cdTau;
    dA[n] = dMidValue + (2. * dThermalConductivityCalc(_cpInputData, cdTbu[n - 1]) /
                         qPow(cdStep, 2));
    dB[n] = dC[n] = dThermalConductivityCalc(_cpInputData, cdTbu[n - 1]) / qPow(cdStep, 2);
    if (cdQ1) {
      dD[n] = cdQ1[n - 1] + (dMidValue * cdT1[n - 1]);
    }
    else {
      dD[n] = dMidValue * cdT1[n - 1];
    }
  }
}
//-------------------------------------------------------------------------------------------------
void AbstractMathModel::vTDMA(const quint16 cusN, const double* cdAi, const double* cdBi,
                              const double* cdCi, const double* cdDi, double* dT) const
{
  double dP[cusN + 1], dQ[cusN + 1];
  dP[0] = cdBi[0] / cdAi[0];
  dQ[0] = cdDi[0] / cdAi[0];
  for (quint16 n = 1; n <= cusN; ++n) {
    dP[n] = cdBi[n] / (cdAi[n] - (cdCi[n] * dP[n - 1]));
    dQ[n] = (cdDi[n] + (cdCi[n] * dQ[n - 1])) / (cdAi[n] - (cdCi[n] * dP[n - 1]));
  }
  dT[cusN] = dQ[cusN];
  for (qint16 n = cusN - 1; n >= 0; --n) {
    dT[n] = (dP[n] * dT[n + 1]) + dQ[n];
  }
}
//-------------------------------------------------------------------------------------------------
void AbstractMathModel::vPlotPause() const
{
  QEventLoop oEventLoopUntilPlot;
  connect(this, &AbstractMathModel::signalResumePlotter, &oEventLoopUntilPlot, &QEventLoop::quit);
  oEventLoopUntilPlot.exec();
}
//-------------------------------------------------------------------------------------------------
void AbstractMathModel::vTryPause() const
{
  if (_bPause) {
    QEventLoop oEventLoopUserPause;
    connect(this, &AbstractMathModel::signalResumeUser, &oEventLoopUserPause, &QEventLoop::quit);
    oEventLoopUserPause.exec();
  }
}
//-------------------------------------------------------------------------------------------------
}
