#include "MathModel2D.h"
#include "InputData.h"
#include "Thermodynamic.h"
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
MathModel2D::MathModel2D(const std::shared_ptr<InputData> cpInputData, QObject* pParent):
  AbstractMathModel(cpInputData, pParent),
  _dTY1(new double [cpInputData->_usNObjWidth - 1])
{
  _dTXY.resize(cpInputData->_usNObjWidth + 1);
  _dTXY.fill(QVector<double>(cpInputData->_usNObjLenght + 1, cpInputData->_dObjTemp));
  _dTXYbu = _dTXY;
}
//-------------------------------------------------------------------------------------------------
MathModel2D::~MathModel2D()
{
  delete [] _dTY1;
}
//-------------------------------------------------------------------------------------------------
void MathModel2D::slotProcess()
{
  // признак достижения температыры плавления
  bool bFusionTemp{false};
  // послать результат нулевой итерации
  emit signalIterationComplete(_iPercentComplite, _fCurrentTime, &_dTXY);
  // расчет по направлениям
  for (quint16 t = 0; t < _cpInputData->_usNTime; ++t) {
    // направление 0X
    for (quint16 j = 1; j < _cpInputData->_usNObjLenght; ++j) {
      for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
        // срез температур для расчета прогоночных коэффициентов по оси OX
        _dTY1[i - 1] = _dTXY[i][j];
        // срез мощностей для расчета прогоночных коэффициентов по оси OX
        _dQ1[i - 1] = _dQXY[i][j];
      }
      // расчет прогоночных коэффициентов по оси OX
      vABCD(_cpInputData->_usNObjWidth, _dTXY[0][j], _dTXY[_cpInputData->_usNObjWidth][j],
            _dAxi, _dBxi, _dCxi, _dDxi, _dTY1, _dTY1, _dQ1, _cdXStep, _cdTau);
      // трехточечная прогонка
      vTDMA(_cpInputData->_usNObjWidth, _dAxi, _dBxi, _dCxi, _dDxi, _dTX);
      // запись новых значений температур в основной массив текущих температур
      for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
        _dTXY[i][j] = _dTX[i];
      }
    }
    // обновление температур на границах
    for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
      _dTXY[i][0] = _dTXY[i][1];
      _dTXY[i][_cpInputData->_usNObjLenght] = _dTXY[i][_cpInputData->_usNObjLenght - 1];
    }
    // направление 0Y
    for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
      for (quint16 j = 1; j < _cpInputData->_usNObjLenght; ++j) {
        // срезы температур для расчета прогоночных коэффициентов и термодинамических функций по
        // оси OY
        _dTX1[j - 1]  = _dTXY[i][j];
        _dTXbu[j - 1] = _dTXYbu[i][j];
      }
      // расчет прогоночных коэффициентов по оси OY
      vABCD(_cpInputData->_usNObjLenght, _dTXYbu[i][0], _dTXYbu[i][_cpInputData->_usNObjLenght],
            _dAyj, _dByj, _dCyj, _dDyj, _dTX1, _dTXbu, nullptr, _cdYStep, _cdTau);
      // трехточечная прогонка
      vTDMA(_cpInputData->_usNObjLenght, _dAyj, _dByj, _dCyj, _dDyj, _dTY);
      // запись новых значений температур в основной массив текущих температур и
      // проверка достижения температуры плавления
      for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
        _dTXY[i][j] = _dTY[j];
        if (!static_cast<bool>(dDensityCalc(_cpInputData, _dTXY[i][j]))) {
          bFusionTemp = true;
        }
      }
    }
    // обновление температур на границах
    for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
      _dTXY[0][j] = _dTXY[1][j];
      _dTXY[_cpInputData->_usNObjWidth][j] = _dTXY[_cpInputData->_usNObjWidth - 1][j];
    }
    // запись в массив текущих температур значений предыдущей итерации и завершение расчета, если
    // был выствлен признак остановки или достигнута температура плавления
    if (_bStop || bFusionTemp) {
      vBuTemp(_dTXY, _dTXYbu);
      if (bFusionTemp) {
        emit signalTempFusionReach();
      }
      break;
    }
    // запись рассчитанных температур в массив-бэкап на случай, если в следующей итерации будет
    // выствлен признак остановки или будет достигнута температура плавления
    vBuTemp(_dTXYbu, _dTXY);
    // расчет процента выполненного расчета
    _iPercentComplite = static_cast<int>(100. * static_cast<double>(t + 1) /
                        static_cast<double>(_cpInputData->_usNTime));
    // приращение прошедшего времени на шаг
    _fCurrentTime += _cdTau;
    // завершение итерации расчета
    emit signalIterationComplete(static_cast<int>(100. * static_cast<double>(t + 1) /
                                 static_cast<double>(_cpInputData->_usNTime)), _fCurrentTime,
                                 &_dTXY);
    // приостановить расчет на время построения графика
    vPlotPause();
    // приостановить расчет, если был выствлен признак паузы
    vTryPause();
  }
  emit signalFinished();
}
//-------------------------------------------------------------------------------------------------
}
