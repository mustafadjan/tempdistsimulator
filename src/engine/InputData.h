#pragma once
#include <qglobal.h>
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {

/** Тип расчета */
enum class EnDimType
{
  enDim2D = 0,
  enDim3D
};
/** Тип теплового источника */
enum class EnQwType
{
  enQwDot = 0,
  enQwLinear,
  enQwEllipse
};
/** Тип материала (металла) */
enum class EnMaterialType
{
  enMaterialCopper = 0,
  enMaterialSilver,
  enMaterialGold,
  enMaterialZinc,
  enMaterialAluminium,
  enMaterialTin,
  enMaterialLead,
  enMaterialIron,
  enMaterialCobalt,
  enMaterialNickel,
  enMaterialOther
};
//-------------------------------------------------------------------------------------------------
/**
 * @brief Структура входных данных для расчета считываемые с формы
 */
struct InputData final
{

  /** Тип расчета */
  EnDimType _enDimType{EnDimType::enDim2D};

  /** Тип теплового источника */
  EnQwType _enQwType{EnQwType::enQwDot};

  /** Тип материала (металла) */
  EnMaterialType _enMaterialType{EnMaterialType::enMaterialCopper};

  /** Количество шагов по ширине объекта */
  quint16 _usNObjWidth{0};

  /** Количество шагов по длине объекта */
  quint16 _usNObjLenght{0};

  /** Количество шагов по высоте объекта */
  quint16 _usNObjHeight{0};

  /** Количество шагов по времени процесса */
  quint16 _usNTime{0};

  /** Ширина объекта, (м) */
  double _dObjWidth{0.};

  /** Длина объекта, (м) */
  double _dObjLenght{0.};

  /** Высота объекта, (м) */
  double _dObjHeight{0.};

  /** Начальная температура объекта, (K) */
  double _dObjTemp{0.};

  /** Температура окружающей среды, (K) */
  double _dAreaTemp{0.};

  /** Время процесса, (с) */
  double _dTime{0.};

  /** Удельная мощность источника теплового излучения, (Вт/м^2) */
  double _dPowerDensity{0.};

  /** Плотность материала, (кг/м^3) */
  double _dObjDensity{0.};

  /** Удельная теплоемкость материала, (Дж/(кг*K)) */
  double _dObjHeatCapacity{0.};

  /** Теплопроводность материала, (Вт/(м*K)) */
  double _dObjThermalConductivity{0.};

  /** Температура плавления материала, (K) */
  double _dObjFusionTemp{0.};

};
//-------------------------------------------------------------------------------------------------
}
