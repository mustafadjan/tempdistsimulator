#include "MathModel3D.h"
#include "InputData.h"
#include "Thermodynamic.h"
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
MathModel3D::MathModel3D(const std::shared_ptr<InputData> cpInputData, QObject* pParent):
  AbstractMathModel(cpInputData, pParent),
  _cdZStep(cpInputData->_dObjHeight / static_cast<double>(cpInputData->_usNObjHeight)),
  _dTY1   (new double[cpInputData->_usNObjHeight - 1]),
  _dTZ1   (new double[cpInputData->_usNObjWidth  - 1]),
  _dTYbu  (new double[cpInputData->_usNObjHeight - 1]),
  _dTZ    (new double[cpInputData->_usNObjHeight + 1]),
  _dAzk   (new double[cpInputData->_usNObjHeight + 1]),
  _dBzk   (new double[cpInputData->_usNObjHeight + 1]),
  _dCzk   (new double[cpInputData->_usNObjHeight + 1]),
  _dDzk   (new double[cpInputData->_usNObjHeight + 1]),
  _usK    (cpInputData->_usNObjHeight)
{
  _dTXYZ.resize(cpInputData->_usNObjWidth + 1);
  _dTXYZ.fill(QVector<QVector<double>>(cpInputData->_usNObjLenght + 1));
  for (int i = 0; i < cpInputData->_usNObjWidth + 1; ++i) {
    _dTXYZ[i].fill(QVector<double>(cpInputData->_usNObjHeight + 1, cpInputData->_dObjTemp));
  }
  _dTXYZbu = _dTXYZ;
  _dTXY.resize(cpInputData->_usNObjWidth + 1);
  _dTXY.fill(QVector<double>(cpInputData->_usNObjLenght + 1));
}
//-------------------------------------------------------------------------------------------------
MathModel3D::~MathModel3D()
{
  delete [] _dTY1;
  delete [] _dTZ1;
  delete [] _dTYbu;
  delete [] _dTZ;
  delete [] _dAzk;
  delete [] _dBzk;
  delete [] _dCzk;
  delete [] _dDzk;
}
//-------------------------------------------------------------------------------------------------
void MathModel3D::vSendIterationResult(const int ciK)
{
  // срез высотного уровня
  for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
    for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
      _dTXY[i][j] = _dTXYZ[i][j][ciK];
    }
  }
  // завершение итерации расчета
  emit signalIterationComplete(_iPercentComplite, _fCurrentTime, &_dTXY);
}
//-------------------------------------------------------------------------------------------------
void MathModel3D::slotProcess()
{
  // признак достижения температыры плавления
  bool bFusionTemp{false};
  // послать результат нулевой итерации
  vSendIterationResult(static_cast<int>(_usK));
  // расчет по направлениям
  for (quint16 t = 0; t < _cpInputData->_usNTime; ++t) {
    // направление 0X
    for (quint16 j = 1; j < _cpInputData->_usNObjLenght; ++j) {
      for (quint16 k = 1; k < _cpInputData->_usNObjHeight; ++k) {
        for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
          // срез температур для расчета прогоночных коэффициентов по оси OX
          _dTZ1[i - 1] = _dTXYZ[i][j][k];
          // срез мощностей для расчета прогоночных коэффициентов по оси OX
          if (k == _cpInputData->_usNObjHeight - 1) {
            _dQ1[i - 1] = _dQXY[i][j];
          }
          else {
            _dQ1[i - 1] = 0.;
          }
        }
        // расчет прогоночных коэффициентов по оси OX
        vABCD(_cpInputData->_usNObjWidth, _dTXYZ[0][j][k],
              _dTXYZ[_cpInputData->_usNObjWidth][j][k], _dAxi, _dBxi, _dCxi, _dDxi, _dTZ1, _dTZ1,
              _dQ1, _cdXStep, _cdTau, 1.5);
        // трехточечная прогонка
        vTDMA(_cpInputData->_usNObjWidth, _dAxi, _dBxi, _dCxi, _dDxi, _dTX);
        // запись новых значений температур в основной массив текущих температур
        for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
          _dTXYZ[i][j][k] = _dTX[i];
        }
      }
    }
    // обновление температур на границах
    for (quint16 j = 1; j < _cpInputData->_usNObjLenght; ++j) {
      for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
        _dTXYZ[i][j][0] = _dTXYZ[i][j][1];
        _dTXYZ[i][j][_cpInputData->_usNObjHeight] = _dTXYZ[i][j][_cpInputData->_usNObjHeight - 1];
      }
    }
    for (quint16 k = 1; k < _cpInputData->_usNObjHeight; ++k) {
      for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
        _dTXYZ[i][0][k] = _dTXYZ[i][1][k];
        _dTXYZ[i][_cpInputData->_usNObjLenght][k] = _dTXYZ[i][_cpInputData->_usNObjLenght - 1][k];
      }
    }
    for (quint16 i = 0; i <= _cpInputData->_usNObjWidth; ++i) {
      _dTXYZ[i][0][0] = _dTXYZ[i][1][1];
      _dTXYZ[i][0][_cpInputData->_usNObjHeight] = _dTXYZ[i][1][_cpInputData->_usNObjHeight - 1];
      _dTXYZ[i][_cpInputData->_usNObjLenght][0] = _dTXYZ[i][_cpInputData->_usNObjLenght - 1][1];
      _dTXYZ[i][_cpInputData->_usNObjLenght][_cpInputData->_usNObjHeight] =
      _dTXYZ[i][_cpInputData->_usNObjLenght - 1][_cpInputData->_usNObjHeight - 1];
    }
    // направление 0Y
    for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
      for (quint16 k = 1; k < _cpInputData->_usNObjHeight; ++k) {
        for (quint16 j = 1; j < _cpInputData->_usNObjLenght; ++j) {
          // срезы температур для расчета прогоночных коэффициентов и термодинамических функций по
          // оси OY
          _dTX1[j - 1] = _dTXYZ[i][j][k];
          _dTXbu[j - 1] = _dTXYZbu[i][j][k];
        }
        // расчет прогоночных коэффициентов по оси OY
        vABCD(_cpInputData->_usNObjLenght, _dTXYZbu[i][0][k],
              _dTXYZbu[i][_cpInputData->_usNObjLenght][k], _dAyj, _dByj, _dCyj, _dDyj, _dTX1,
              _dTXbu, nullptr, _cdYStep, _cdTau, 1.5);
        // трехточечная прогонка
        vTDMA(_cpInputData->_usNObjLenght, _dAyj, _dByj, _dCyj, _dDyj, _dTY);
        // запись новых значений температур в основной массив текущих температур
        for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
          _dTXYZ[i][j][k] = _dTY[j];
        }
      }
    }
    // обновление температур на границах
    for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
      for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
        _dTXYZ[i][j][0] = _dTXYZ[i][j][1];
        _dTXYZ[i][j][_cpInputData->_usNObjHeight] = _dTXYZ[i][j][_cpInputData->_usNObjHeight - 1];
      }
    }
    for (quint16 k = 1; k < _cpInputData->_usNObjHeight; ++k) {
      for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
        _dTXYZ[0][j][k] = _dTXYZ[1][j][k];
        _dTXYZ[_cpInputData->_usNObjWidth][j][k] = _dTXYZ[_cpInputData->_usNObjWidth - 1][j][k];
      }
    }
    for (quint16 j = 0; j <= _cpInputData->_usNObjLenght; ++j) {
      _dTXYZ[0][j][0] = _dTXYZ[1][j][1];
      _dTXYZ[0][j][_cpInputData->_usNObjHeight] = _dTXYZ[1][j][_cpInputData->_usNObjHeight - 1];
      _dTXYZ[_cpInputData->_usNObjWidth][j][0] = _dTXYZ[_cpInputData->_usNObjWidth - 1][j][1];
      _dTXYZ[_cpInputData->_usNObjWidth][j][_cpInputData->_usNObjHeight] =
      _dTXYZ[_cpInputData->_usNObjWidth - 1][j][_cpInputData->_usNObjHeight - 1];
    }
    // направление 0Z
    for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
      for (quint16 j = 1; j < _cpInputData->_usNObjLenght; ++j) {
        for (quint16 k = 1; k < _cpInputData->_usNObjHeight; ++k) {
          // срезы температур для расчета прогоночных коэффициентов и термодинамических функций по
          // оси OY
          _dTY1[k - 1] = _dTXYZ[i][j][k];
          _dTYbu[k - 1] = _dTXYZbu[i][j][k];
        }
        // расчет прогоночных коэффициентов по оси OZ
        vABCD(_cpInputData->_usNObjHeight, _dTXYZbu[i][j][0],
              _dTXYZbu[i][j][_cpInputData->_usNObjHeight], _dAzk, _dBzk, _dCzk, _dDzk, _dTY1,
              _dTYbu, nullptr, _cdZStep, _cdTau, 1.5);
        // трехточечная прогонка
        vTDMA(_cpInputData->_usNObjHeight, _dAzk, _dBzk, _dCzk, _dDzk, _dTZ);
        // запись новых значений температур в основной массив текущих температур
        for (quint16 k = 0; k <= _cpInputData->_usNObjHeight; ++k) {
          _dTXYZ[i][j][k] = _dTZ[k];
          if (!static_cast<bool>(dDensityCalc(_cpInputData, _dTXYZ[i][j][k]))) {
            bFusionTemp = true;
          }
        }
      }
    }
    // обновление температур на границах
    for (quint16 i = 1; i < _cpInputData->_usNObjWidth; ++i) {
      for (quint16 k = 0; k <= _cpInputData->_usNObjHeight; ++k) {
        _dTXYZ[i][0][k] = _dTXYZ[i][1][k];
        _dTXYZ[i][_cpInputData->_usNObjLenght][k] = _dTXYZ[i][_cpInputData->_usNObjLenght - 1][k];
      }
    }
    for (quint16 j = 1; j < _cpInputData->_usNObjHeight; ++j) {
      for (quint16 k = 0; k <= _cpInputData->_usNObjHeight; ++k) {
        _dTXYZ[0][j][k] = _dTXYZ[1][j][k];
        _dTXYZ[_cpInputData->_usNObjWidth][j][k] = _dTXYZ[_cpInputData->_usNObjWidth - 1][j][k];
      }
    }
    for (quint16 k = 0; k <= _cpInputData->_usNObjHeight; ++k) {
      _dTXYZ[0][0][k] = _dTXYZ[1][1][k];
      _dTXYZ[0][_cpInputData->_usNObjLenght][k] = _dTXYZ[1][_cpInputData->_usNObjLenght - 1][k];
      _dTXYZ[_cpInputData->_usNObjWidth][0][k] = _dTXYZ[_cpInputData->_usNObjWidth - 1][1][k];
      _dTXYZ[_cpInputData->_usNObjWidth][_cpInputData->_usNObjLenght][k] =
      _dTXYZ[_cpInputData->_usNObjWidth - 1][_cpInputData->_usNObjLenght - 1][k];
    }
    // запись в массив текущих температур значений предыдущей итерации и завершение расчета, если
    // был выствлен признак остановки или достигнута температура плавления
    if (_bStop || bFusionTemp) {
      vBuTemp(_dTXYZ, _dTXYZbu);
      if (bFusionTemp) {
        emit signalTempFusionReach();
      }
      break;
    }
    // запись рассчитанных температур в массив-бэкап на случай, если в следующей итерации будет
    // выствлен признак остановки или будет достигнута температура плавления
    vBuTemp(_dTXYZbu, _dTXYZ);
    // расчет процента выполненного расчета
    _iPercentComplite = static_cast<int>(100. * static_cast<double>(t + 1) /
                        static_cast<double>(_cpInputData->_usNTime));
    // приращение прошедшего времени на шаг
    _fCurrentTime += _cdTau;
    // послать результат расчета итерации
    vSendIterationResult(static_cast<int>(_usK));
    // приостановить расчет, если был выствлен признак паузы
    vTryPause();
  }
  emit signalSendFinalArray(&_dTXYZ);
}
//-------------------------------------------------------------------------------------------------
}
