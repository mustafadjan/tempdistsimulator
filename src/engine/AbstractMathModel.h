#pragma once
//-------------------------------------------------------------------------------------------------
#include "Typedefs.h"
#include <memory>
#include <QObject>
//-------------------------------------------------------------------------------------------------
namespace TempDistSimulatorEngine {
//-------------------------------------------------------------------------------------------------
struct InputData;
//-------------------------------------------------------------------------------------------------
/**
 * @brief Базовый абстрактный класс математической модели
 * @details Позволяет рассчитать распределение температур в объекте по входным данным
 */
class AbstractMathModel : public QObject
{
  Q_OBJECT

public:

  /** @brief Конструктор */
  explicit AbstractMathModel(const std::shared_ptr<InputData>, QObject* pParent = nullptr);

  /** @brief Деструктор */
  ~AbstractMathModel();

  /** @brief Установка флага о приостановке расчета */
  void vPause(bool b) { _bPause = b; }

  /** @brief Установка флага о завершении расчета */
  void vStop() { _bStop = true; }

  /** @brief Проверка флага о приостановке расчета */
  bool bIsPaused() const { return _bPause; }

protected:

  /** @brief Расчет прогоночных коэффициентов по оси ON */
  void vABCD(const quint16, const double, const double, double*, double*, double*, double*,
             const double*, const double*, const double*, const double, const double,
             const double cdDimCoef = 1.) const;

  /** @brief Трехточечная прогонка */
  void vTDMA(quint16, const double*, const double*, const double*, const double*, double*) const;

  /** @brief Приостановка расчета на время построения графика */
  void vPlotPause() const;

  /** @brief Приостановка расчета по требованию пользователя */
  void vTryPause() const;

  /** @brief Бэкап последней итерации массива температур */
  template <typename T>
  void vBuTemp(T& t1, const T& ct2) { t1 = ct2; }

  /** @brief Входные данные для расчета считываемые с формы */
  const std::shared_ptr<InputData> _cpInputData;

  /** @brief Удельная мощность источника теплового излучения */
  double **_dQXY;

  /** @brief Шаг по ширине объекта */
  const double _cdXStep;

  /** @brief Шаг по длине объекта */
  const double _cdYStep;

  /** @brief Шаг по времени */
  const double _cdTau;

  /** @brief Срез температур для расчета прогоночных коэффициентов */
  double *_dTX1;

  /** @brief Срез температур для расчета термодинамических функций */
  double *_dTXbu;

  /** @brief Срезы температур для записи результатов трехточечной прогонки и мощностей */
  double *_dTX, *_dTY, *_dQ1;

  /** @brief Коэффициенты для расчета температур */
  double *_dAxi, *_dBxi, *_dCxi, *_dDxi, *_dAyj, *_dByj, *_dCyj, *_dDyj;

  /** @brief Флаг о приостановке расчета */
  bool _bPause;

  /** @brief Флаг о завершении расчета */
  bool _bStop;

  /** @brief Процент выполненного расчета */
  int _iPercentComplite;

  /** @brief Текущее время */
  float _fCurrentTime;

public slots:

  /** @brief Запуск расчета */
  virtual void slotProcess() = 0;

signals:

  /** @brief Завершение итерации расчета */
  void signalIterationComplete(const int, const float, const ArrayDouble2D*) const;

  /** @brief Возобновление расчета после построения графика */
  void signalResumePlotter() const;

  /** @brief Возобновление расчета по требованию пользователя */
  void signalResumeUser() const;

  /** @brief Достигнута темпелатура плавления */
  void signalTempFusionReach() const;

};
//-------------------------------------------------------------------------------------------------
}
